<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>
	
<!-- Page de confirmation de la suppression d'un commentaire -->
<!-- auteur : Miary -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<!-- LISTE DES VIDEOS SELON LE GENRE ET LA CATEGORIE -->
		<div class="main-grids news-main-grids">
			<div class="recommended-info"></div>
			<!-- Récuperer le message -->
			<c:set var="msg" value="${msg}" />
			<h2>
				<c:out value="${msg}" />
			</h2>
		</div>
	</div>
</div>