<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Recuperation des parametres -->
<c:set var="erreur_paiement" value="${erreur_paiement}" />
<c:set var="res_achat" value="${res_achat}" />

<!-- Renvoi d'une page de confirmation si l'achat a bien �t� effectu� avec succ�s ou non -->
<!-- auteur : Miary -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="col-sm-10 show-grid-left main-grids">
			<div class="recommended">
				<div class="recommended-grids english-grid">
					<div class="recommended-info">
						<c:if test="${not empty erreur_paiement}">
							<div class="recommended-info">
								<h3>
									<c:out value="${erreur_paiement}" />
								</h3>
							</div>
						</c:if>
						<c:if test="${res_achat}">
							<div class="recommended-info">
								<h3>Achat r�ussi, vous pouvez t�l�charger :</h3>
								<div class="signup">
									<form action="download" method=post>
										<input type="submit" name="submit"
											value="T�l�charger la vid�o" /> <br>
									</form>
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>