<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- En-tête de page que nous avons incluse dans les autres pages avec la fonction include -->
<!-- auteur : Miary -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Netflox</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords"
		content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
	<script type="application/x-javascript">
		 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
	</script>
	<!-- bootstrap -->
	<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'
		media="all" />
	<!-- //bootstrap -->
	<link href="css/dashboard.css" rel="stylesheet">
		<!-- Custom Theme files -->
		<link href="css/style.css" rel='stylesheet' type='text/css'
			media="all" />
		<script src="js/jquery-1.11.1.min.js"></script>
		<!--start-smoth-scrolling-->
		<!-- fonts -->
		<link
			href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
			rel='stylesheet' type='text/css'>
			<link href='//fonts.googleapis.com/css?family=Poiret+One'
				rel='stylesheet' type='text/css'>
				<!-- //fonts -->
			
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><h1>
					<img src="images/logo.png" alt="" />
				</h1></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		
			<!-- FORMULAIRE RECHERCHE DEBUT -->
			<div class="top-search">
				<form method="post" action="search"	class="navbar-form navbar-right">
					<input type="text" class="form-control"	placeholder="Rechercher une vidéo..." name="Recherche"/>
					<input type="submit" value=""/>
				</form>
			</div>
			<!-- FORMULAIRE RECHERCHE FIN -->
			<c:set var="TYPEu" value="${TYPEu}" />

			<div class="header-top-right">
				<!--<div class="file">
					<a href="upload.html">Upload</a>
				</div>	-->
				<c:if test="${empty TYPEu}">
				<div class="signin">
					<!-- <a href="#small-dialog2" class="play-icon popup-with-zoom-anim">Créer un compte</a> -->
					<a href="addClt.jsp" >Créer un compte</a>
					<!-- pop-up-box -->
					<script type="text/javascript" src="js/modernizr.custom.min.js"></script>
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css"
						media="all" />
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<script>
						$(document).ready(function() {
							$('.popup-with-zoom-anim').magnificPopup({
								type : 'inline',
								fixedContentPos : false,
								fixedBgPos : true,
								overflowY : 'auto',
								closeBtnInside : true,
								preloader : false,
								midClick : true,
								removalDelay : 300,
								mainClass : 'my-mfp-zoom-in'
							});

						});
					</script>
				</div>
				<div class="signin">
					<!-- <a href="#small-dialog" class="play-icon popup-with-zoom-anim">Se connecter</a> -->
					<a href="connexion.jsp" >Se connecter</a>
					<div id="small-dialog" class="mfp-hide">
						<h3>Se connecter</h3>
						<div class="social-sits">
							<div class="facebook-button">
								<a href="#">Se connecter avec Facebook</a>
							</div>
							<div class="chrome-button">
								<a href="#">Se connecter avec Google</a>
							</div>
							<div class="button-bottom">
								<p>
									Nouveau client? <a href="#small-dialog2"
										class="play-icon popup-with-zoom-anim">Inscrivez-vous</a>
								</p>
							</div>
						</div>
						<div class="signup">
							<form>
								<input type="text" class="email"
									placeholder="Entrez votre adresse e-mail/numéro de téléphone"
									required="required" pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?" />
								<input type="password" placeholder="Mot de passe"
									required="required" pattern=".{6,}"
									title="Longueur minimum : 6 caractères" autocomplete="off" />
								<input type="submit" value="Je me connecte" />
							</form>
							<div class="forgot">
								<a href="#">Mot de passe oublié ?</a>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				</c:if>
				
				<!-- DEBUT ENTETE CLIENT ET ADMINISTRATEUR -->
				<c:if test="${! empty TYPEu}">
				<c:if test="${TYPEu != 'Administrateur'}">
					<div class="signin">
						<a href="accueil_adminclient.jsp">Mon compte</a>
						<div class="clearfix"></div>
					</div>
					<div class="signin">
						<a href="deconnexion">Déconnexion</a>
						<div class="clearfix"></div>
					</div>
				</c:if>
				<c:if test="${TYPEu == 'Administrateur'}">
					<div class="signin">
						<a href="accueil_adminclient.jsp">Administration</a>
						<div class="clearfix"></div>
					</div>
					<div class="signin">
						<a href="deconnexion">Déconnexion</a>
						<div class="clearfix"></div>
					</div>
				</c:if>
				</c:if>
				<!-- FIN ENTETE CLIENT ET ADMINISTRATEUR -->
				
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	</nav>
	</body>
	</html> 