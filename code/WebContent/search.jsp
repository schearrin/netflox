<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Affichage des r�sultats d'une recherche de vid�o -->
<!-- auteur : Sophie -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="main-grids news-main-grids">
			<div class="recommended-info">
				<h3>R�sultats de votre recherche :</h3>
			</div>
			<c:choose>
				<c:when test="${! empty res_recherche}">
					<c:forEach var="p" items="${res_recherche}">
					<!-- test -->
						<!-- <c:out value="${p.value['titre']}" /> -->
						<!-- test fin -->
						<!-- affiche une video -->
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="video?id=${p.key}"><img
									src="images/videos/<c:out value="${p.value['categorie']}" />/<c:out value="${p.value['nom_img']}" />.jpg"
									alt="" /></a>
							</div>
							<div
								class="resent-grid-info recommended-grid-info video-info-grid">
								<h5>
									<a href="video.jsp" class="title"><c:out
											value="${p.value['titre']}" /></a>
								</h5>
							</div>
						</div>
						<!-- fin affiche une video -->
					</c:forEach>
				</c:when>
				<c:otherwise>
				<h4>Aucun r�sultat ne correspond � votre recherche.</h4>
				</c:otherwise>
			</c:choose>
	<div class="clearfix"></div>

		</div>
	</div>

</div>
