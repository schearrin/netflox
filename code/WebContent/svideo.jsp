<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Affichage d'un message de confirmation si la vid�o a bien �t� supprim�e ou non -->
<!-- auteur : Miary -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="main-grids news-main-grids">

			<div class="recommended-info"></div>

			<!-- R�cuperer le message -->
			<c:set var="res_delvideo" value="${res_delvideo}" />
			
			<c:if test="${res_delvideo == true}">
				<h2>La vid�o a bien �t� supprim�.</h2>
			</c:if>
			<c:if test="${res_delvideo == false}">
				<h2>La vid�o n'existe pas ou a d�j� �t� supprim�.</h2>
			</c:if>
		</div>
	</div>
</div>