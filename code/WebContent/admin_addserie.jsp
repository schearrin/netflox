<link href="css/popuo-box.css" rel="stylesheet" type="text/css"
	media="all" />
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Formulaire � remplir par l'administrateur s'il souhaite ajouter une s�rie -->
<!-- auteur : Miary -->

<c:set var="res_addvideoserie" value="${res_addvideoserie}" />

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<div class="recommended-info">
				<c:if test="${res_addvideoserie == true}">
					<h2>La vid�o a bien �t� ajout�e.</h2>
					<br>
					<br>
				</c:if>
				<c:if test="${empty res_addvideoserie}">
					<h2>Pour ajouter une s�rie :</h2>
				</c:if>
			</div>
		</div>

		<c:if test="${empty res_addvideoserie}">
			<div class="signup">
				<form action="admin_addserie" method="post" enctype="multipart/form-data">
					<h4>S�lectionnez une vid�o</h4>

					<input style="width: 90%; padding: 10px; font-size: 14px;"
						type="file" id="fichier_video" name="fichier_video"> <input
						type="hidden" name="taille_max" value="10000"> <br /> <br />

					<input type="text" class="email" placeholder="Titre" name="titre"
						required="required" title="Entrez le titre de la vid�o" /> <input
						type="text" class="email" placeholder="Auteur" name="auteur"
						required="required" title="Entrez le nom du r�alisateur" /> <input
						type="text" class="email" placeholder="Ann�e de sortie"
						name="annee" required="required"
						title="Choisissez son ann�e de sortie" /> <input type="text"
						class="email" placeholder="Prix d'achat" name="prix"
						required="required" title="Entrez le prix d'achat de la vid�o" />

					<select style="width: 90%; padding: 10px; font-size: 14px;"
						name="etat" id="etat">
						<option value="Disponible">Disponible</option>
						<option value="Non disponible">Non-disponible</option>
					</select> <br /> <br /> <select
						style="width: 90%; padding: 10px; font-size: 14px;"
						name="categorie" id="categorie">
						<option value="Serie">S�rie</option>
					</select> <br /> <br /> <select
						style="width: 90%; padding: 10px; font-size: 14px;" name="genre"
						id="genre">
						<optgroup label="S�lectionnez le genre">
							<option value="Action">Action</option>
							<option value=Policier>Policier</option>
							<option value="Romantique">Romantique</option>
							<option value="Sc-Fiction">Science-Fiction</option>
						</optgroup>
					</select> <input type="text" class="email" placeholder="Entrez la saison"
						name="saison" required="required" title="Saison" /> <input
						type="text" class="email"
						placeholder="Entrez le num�ro de l'�pisode" name="episode"
						required="required" title="Episode" /> <input type="text"
						class="email" placeholder="Mots-cl�s" name="motscles"
						required="required" title="Entrez des mots-cl�s pour la vid�o" />

					<!-- 	<input type="text"
					class="email" placeholder="Nom image" name="nom_img"
					required="required" title="Choisissez une image" /> <br /> <br />-->

					<h4>S�lectionnez une image</h4>
					<input style="width: 90%; padding: 10px; font-size: 14px;"
						type="file" id="nom_img" name="nom_img"> <input
						type="hidden" name="taille_max" value="10000"> <br /> <br />

					<textarea style="width: 90%; padding: 10px; font-size: 14px;"
						name="synopsis" placeholder="R�sum� de la vid�o"></textarea>

					<br /> <br /> <input type="submit" value="Ajouter vid�o" />

				</form>
			</div>
		</c:if>
	</div>
	<div class="clearfix"></div>
</div>