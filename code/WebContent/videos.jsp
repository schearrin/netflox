<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<!-- LISTE DES VIDEOS SELON LE GENRE ET LA CATEGORIE -->
<!-- 		auteur : Sophie -->
		<div class="main-grids news-main-grids">
			<div class="recommended-info">
				<h3>Liste des vid�os</h3>
			</div>
			<%
				//Methode 1 pour recup les parametres :
				String cat = (String) request.getAttribute("param_categorie");
				String gen = (String) request.getAttribute("param_genre");
			%>
			<c:choose>
				<c:when test="${! empty res_videos }">
					<c:forEach var="p" items="${res_videos}">
						<!-- affiche une video -->
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="video?id=${p.key}"><img
									src="images/videos/<%out.println(cat);%>/<c:out value="${p.value['nom_img']}" />"
									alt="" /></a>
							</div>
							<div
								class="resent-grid-info recommended-grid-info video-info-grid">
								<h5>
									<a href="video.jsp" class="title"><c:out
											value="${p.value['titre']}" /></a>
								</h5>
							</div>
						</div>
						<!-- fin affiche une video -->
					</c:forEach>
				</c:when>
			</c:choose>
			<div class="clearfix"></div>


			<!-- affiche les pages precedentes ou suivantes  -->

			<!-- Methode 2 pour recuperer parametres :  -->
			<c:set var="nbVideo" value="${nbVideo}" />
			<c:set var="categorie" value="${param_categorie}" />
			<c:set var="genre" value="${param_genre}" />

			<c:set var="from1" value="${from - 10}" />
			<c:set var="from2" value="${from + 10}" />
			<c:set var="to1" value="${to - 10}" />
			<c:set var="to2" value="${to + 10}" />

			<div class="element-ct" style="text-align: center">
				<c:if test="${from1 >= 0 }">
					<a
						href="videos?categorie=${categorie}&genre=${genre}&from=${from1}&to=${to1}">Page
						pr�c�dente</a>
				</c:if>
				<c:if test="${from2 < nbVideo }">
					<a
						href="videos?categorie=${categorie}&genre=${genre}&from=${from2}&to=${to2}">Page
						suivante</a>
				</c:if>
			</div>
			<!-- fin affiche les pages precedentes ou suivantes -->
		</div>
	</div>
	<br>
<%@include file="footer-grid.jsp"%>
</div>
<%@include file="footer.jsp"%>