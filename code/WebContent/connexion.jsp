<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Page de connexion et renvoi d'une confirmation si la connexion s'était effectuée normalement ou non -->
<!-- auteur : Tongasoa -->

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<div class="recommended-info"></div>

			<c:set var="msg" value="${msg}" />

			<c:if test="${! empty msg}">
				<h2>
					<c:out value="${msg}" />
				</h2>
			</c:if>

			<c:if test="${empty msg}">
				<c:if
					test="${ !empty sessionScope.login && !empty sessionScope.mdp }">
					<h2>Salut ${ sessionScope.login } !</h2>
					<br />
					<h2>Bienvenue sur Netflox.</h2>
					<br />

				</c:if>


				<c:if test="${ empty sessionScope.login || empty sessionScope.mdp }">
					<h3>Se connecter</h3>
					<div class="signup">
						<form method="post" action="connexion">
							<fieldset>
								<input type="text" id="login" name="login" class="email"
									placeholder="Login" required="required"
									title="Entrez un login valide" value="" size="20"
									maxlength="60" /> <span class="erreur">${form.erreurs['email']}</span>
								<input type="password" id="mdp" name="mdp"
									placeholder="Mot de passe" required="required" pattern=".{6,}"
									title="Longueur minimum : 6 caractères" autocomplete="off"
									value="" size="20" maxlength="20" /> <span class="erreur">${form.erreurs['motdepasse']}</span>

								<input type="submit" value="Je me connecte" />
								<p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
							</fieldset>
						</form>
						<div class="button-bottom">
							<p>
								Nouveau client? <a href="addClt.jsp">Inscrivez-vous</a>
							</p>
						</div>

					</div>
				</c:if>
			</c:if>
			<div class="clearfix"></div>


			<script src="js/bootstrap.min.js"></script>

		</div>
	</div>
</div>
</body>
</html>