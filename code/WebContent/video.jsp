<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- Affichage d'une vidéo -->
<!-- auteurs : Miary et Sophie -->
<!-- Recupere parametre :  -->
<c:set var="id" value="${id}" />
<c:set var="nbVueVideo" value="${nbVueVideo}" />
<c:set var="nbTelechargementVideo" value="${nbTelechargementVideo}" />
<c:set var="IDu" value="${IDu}" />
<c:set var="TYPEu" value="${TYPEu}" />
<c:set var="si_locationexiste" value="${si_locationexiste}" />

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">

		<div class="col-sm-8 single-left">
			<c:choose>
				<c:when test="${! empty res_detailvideo }">
					<c:forEach var="p" items="${res_detailvideo}">
						<!-- affiche les informations pour une video -->
						<div class="song">
							<div class="song-info">
								<h3>
									<c:out value="${p.value['titre']}" />
									(
									<c:out value="${p.value['etat']}" />
									)
								</h3>
							</div>
							<!-- debut affiche la video -->
							<c:if test="${empty TYPEu}">
								<div class="video-grid">
									<img src="video/seconnecter.jpg" alt="" height="400"
										width="100%" />
								</div>
							</c:if>
							<c:if test="${! empty TYPEu}">
								<c:choose>
									<c:when test="${TYPEu == 'Non premium'}">
										<c:if test="${si_locationexiste}">
											<c:if test="${p.value['etat'] =='Non disponible'}">
												<div class="video-grid">
													<img src="video/nondisponible.jpg" alt="" height="400"
														width="100%" />
												</div>
											</c:if>
											<c:if test="${p.value['etat'] =='Disponible'}">
												<div class="video-grid">
													<EMBED
														SRC="video\<c:out value="${p.value['nom_video']}" />"
														loop="1" height="400" width="100%" autostart="true"></EMBED>
												</div>
											</c:if>
										</c:if>
										<c:if test="${si_locationexiste == false}">
											<c:if test="${p.value['etat'] =='Disponible'}">
												<div class="video-grid">
													<img src="video/fautlouer.jpg" alt="" height="400"
														width="100%" />
												</div>
											</c:if>
											<c:if test="${p.value['etat'] =='Non disponible'}">
												<div class="video-grid">
													<img src="video/nondisponible.jpg" alt="" height="400"
														width="100%" />
												</div>
											</c:if>

										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${p.value['etat'] =='Non disponible'}">
											<div class="video-grid">
												<img src="video/nondisponible.jpg" alt="" height="400"
													width="100%" />
											</div>
										</c:if>
										<c:if test="${p.value['etat'] =='Disponible'}">
											<div class="video-grid">
												<EMBED
													SRC="video\<c:out value="${p.value['nom_video']}" />"
													loop="1" height="400" width="100%" autostart="true"></EMBED>
											</div>
										</c:if>
									</c:otherwise>
								</c:choose>
							</c:if>
							<!-- fin affiche la video -->
						</div>
						<div class="song-grid-right">
							<div class="share">
								<h5>Actions :</h5>
								<ul>
									<c:if test="${TYPEu =='Non premium'}">

										<c:if test="${si_locationexiste == false}">
											<c:if test="${p.value['etat'] =='Disponible'}">
												<li><a
													href="client_location.jsp?action=Location&id=${id}"
													class="icon fb-icon">Louer</a></li>
											</c:if>
										</c:if>

										<li><a href="client_achat.jsp?action=Achat&id=${id}"
											class="icon twitter-icon">Acheter</a></li>
									</c:if>

									<c:if test="${TYPEu =='Premium'}">
										<li><a href="client_achat.jsp?action=Achat&id=${id}"
											class="icon twitter-icon">Acheter</a></li>
									</c:if>
								</ul>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="published">
							<script src="jquery.min.js"></script>
							<script>
					$(document).ready(function() {
						size_li = $("#myList li").size();
						x = 1;
						$('#myList li:lt(' + x + ')').show();
						$('#loadMore').click(function() {
							x = (x + 1 <= size_li) ? x + 1 : size_li;
							$('#myList li:lt(' + x + ')').show();
						});
						$('#showLess').click(function() {
							x = (x - 1 < 0) ? 1 : x - 1;
							$('#myList li').not(':lt(' + x + ')').hide();
						});
					});	
							</script>
							<div class="load_more">
								<ul id="myList">
									<li>
										<h5>Description de la vidéo :</h5>
										<p>
											Id de la vidéo :
											<c:out value="${id}" />
											<br> Titre :
											<c:out value="${p.value['titre']}" />
											<br> Auteur :
											<c:out value="${p.value['auteur']}" />
											<br> Prix d'achat :
											<c:out value="${p.value['prix']}" />
											€ <br> Prix de location : 2 € <br> Etat :
											<c:out value="${p.value['etat']}" />
										</p>
										<h5>Synopsis :</h5>
										<p>
											<c:out value="${p.value['synopsis']}" />
										</p>
										<h5>Statistiques de la vidéo :</h5>
										<p>
											Nombre de vues :
											<c:out value="${nbVueVideo}" />
											<br> Nombre de téléchargement :
											<c:out value="${nbTelechargementVideo}" />
										</p>
									</li>
								</ul>
							</div>
						</div>
						<!-- fin affiche les informations pour une video -->
					</c:forEach>
				</c:when>
			</c:choose>
			<%@include file="commentaire.jsp"%>
		</div>

		<div class="col-md-4 single-right">
			<h3>Vidéos suggérées</h3>


			<c:choose>
				<c:when test="${! empty res_suggestionvideo }">
					<c:forEach var="sug" items="${res_suggestionvideo}">
						<!-- debut affiche video suggestion -->
						<div class="single-grid-right">
							<div class="single-right-grids">
								<div class="col-md-4 single-right-grid-left">
									<a href="video?id=${sug.key}"><img src="images/videos/<c:out value="${sug.value['categorie']}" />/<c:out value="${sug.value['nom_img']}" />" alt="" /></a>
								</div>
								<div class="col-md-8 single-right-grid-right">
									<a href="video?id=${sug.key}" class="title"> <c:out value="${sug.value['titre']}" /></a>
									<p class="author">
										<a href="#" class="author"><c:out value="${sug.value['auteur']}" /></a>
									</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- fin affiche video suggestion -->
					</c:forEach>
				</c:when>
			</c:choose>

		</div>
		<div class="clearfix"></div>
	</div>
	<br>
<%@include file="footer-grid.jsp"%>
</div>
<%@include file="footer.jsp"%>