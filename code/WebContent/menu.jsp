<!-- Menu lat�ral qui s'affiche sur la partie gauche des pages  -->
<!-- auteur : Miary -->

<div class="col-sm-3 col-md-2 sidebar">
	<div class="top-navigation">
		<div class="t-menu">MENU</div>
		<div class="t-img">
			<img src="images/lines.png" alt="" />
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="drop-navigation drop-navigation">
	
		<!-- DEBUT BARRE DE NAVIGATION -->
		<ul class="nav nav-sidebar">
			<li class="active"><a href="index" class="home-icon"><span
					class="glyphicon glyphicon-home" aria-hidden="true"></span>Accueil</a></li>
			<!-- <li><a href="shows.html" class="user-icon"><span class="glyphicon glyphicon-home glyphicon-blackboard" aria-hidden="true"></span>S�ries</a></li>-->
			<!-- <li><a href="history.html" class="sub-icon"><span class="glyphicon glyphicon-home glyphicon-hourglass" aria-hidden="true"></span>Documentaires</a></li>-->
			<li><a href="#" class="menu"><span
					class="glyphicon glyphicon-film" aria-hidden="true"></span>Films<span
					class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-1">
				<li><a href="videos?categorie=Film&genre=Action">Action</a></li>
				<li><a href="videos?categorie=Film&genre=Policier">Policier</a></li>
				<li><a href="videos?categorie=Film&genre=Romantique">Romantique</a></li>
				<li><a href="videos?categorie=Film&genre=Sc-Fiction">Sciences-fiction</a></li>
			</ul>
			<!-- script-for-menu -->
			<script>
					$("li a.menu").click(function() {
						$("ul.cl-effect-1").slideToggle(300, function() {
							// Animation complete.
						});
					});
			</script>

			<!-- SERIES -->
			<li><a href="#" class="menu1"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>S�ries<span
					class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-2">
				<li><a href="videos?categorie=Serie&genre=Action">Action</a></li>
				<li><a href="videos?categorie=Serie&genre=Policier">Policier</a></li>
				<li><a href="videos?categorie=Serie&genre=Romantique">Romantique</a></li>
				<li><a href="videos?categorie=Serie&genre=Sc-Fiction">Sciences-fiction</a></li>
			</ul>
			<!-- script-for-menu -->
			<script>
					$("li a.menu1").click(function() {
						$("ul.cl-effect-2").slideToggle(300, function() {
							// Animation complete.
						});
					});
			</script>

			<!-- DOCUMENTAIRE -->
			<li><a href="#" class="menu2"><span
					class="glyphicon glyphicon-home glyphicon-hourglass"
					aria-hidden="true"></span>Documentaires<span
					class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-3">
				<li><a href="videos?categorie=Documentaire&genre=Animalier">Animalier</a></li>
				<li><a href="videos?categorie=Documentaire&genre=Biographie">Biographie</a></li>
				<li><a href="videos?categorie=Documentaire&genre=Nature">Nature</a></li>
				<li><a href="videos?categorie=Documentaire&genre=Styledevie">Style de vie</a></li>
			</ul>
			<!-- script-for-menu -->
			<script>
					$("li a.menu2").click(function() {
						$("ul.cl-effect-3").slideToggle(300, function() {
							// Animation complete.
						});
					});
			</script>


			<li><a href="" class="news-icon"><span
					class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Contactez-nous</a></li>
		</ul>
		<!-- FIN BARRE DE NAVIGATION -->
		<!-- script-for-menu -->
	 	<script>
				$(".top-navigation").click(function() {
					$(".drop-navigation").slideToggle(300, function() {
						// Animation complete.
					});
				});
		</script>
			
		
		<div class="side-bottom">
			<div class="copyright">
				<p>Copyright � 2016 Universit� Paris-Saclay MIAGE M1</p>
			</div>
		</div>
	</div>
</div>