<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!--
	Formulaire pour supprimer un commentaire
	auteur : Miary
	 -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="col-sm-10 show-grid-left main-grids">
			<div class="recommended">
				<div class="recommended-grids english-grid">
					<div class="recommended-info">
						<div class="signup">
							<form action="admin_ok_delcomm" method=post>
								<h3>Pour supprimer un commentaire</h3>
								<input type="text" class="email"
									placeholder="Entrez l'identifiant du commentaire � supprimer"
									name="id_utilisateur" required="required"
									title="Entrez l'identifiant du commentaire" /> 
								<input type="submit" value="Supprimer" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@include file="menuadmin.jsp"%>
		<div class="clearfix"></div>
	</div>
</div>