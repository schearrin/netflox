<link href="css/popuo-box.css" rel="stylesheet" type="text/css"
	media="all" />
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>
<!-- 
	modClt.jsp
	@author lanson 
	Formulaire pour modifier un client
	 -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="col-sm-10 show-grid-left main-grids">
			<div class="recommended">
				<div class="recommended-grids english-grid">
					<div class="recommended-info">
					<h2>Pour modifier un client : </h2>
					</div>
						<div class="signup">
							<form action="mclient" method=post>
								<input type="text" class="email"
									placeholder="Entrez l'ID � modifier" name="id_utilisateur"
									required="required" title="ID" /> <input type="text"
									class="email" placeholder="Entrez le nom" name="nom"
									required="required" title="nom" /> <input type="text"
									class="email" placeholder="Entrez le pr�nom" name="prenom"
									required="required" title="prenom" /> <input type="text"
									class="email" placeholder="Entrez l'adresse e-mail"
									name="adresse" required="required"
									pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?" title="email" /> <input
									type="text" class="email"
									placeholder="Entrez le num�ro de t�l�phone" name="telephone"
									maxlength="10" pattern="[0-9]{1}\d{9}" title="telephone" /> <input
									type="text" class="email" placeholder="Login" name="login"
									required="required" title="Entrez votre login" /> <input
									type="password" placeholder="Mot de passe" name="mdp"
									required="required" pattern=".{6,}"
									title="Longueur minimum : 6 caract�res" autocomplete="off" />

								<label for="Prem">
									<h4>changer l'�tat du client ?</h4>
								</label> <br /> <select style="width:90%; padding:10px; font-size:14px;" name="type" id="type">
									<option value="Premium">Premium</option>
									<option value="Non premium">Non premium</option>
									<option value="Admistrateur">Admistrateur</option>
								</select> <br /> <input type="submit" value="modifier le client" /> <br />
							</form>
						</div>
					</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
