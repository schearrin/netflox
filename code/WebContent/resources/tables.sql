------------------------------------------
-- COMMANDES
------------------------------------------

-- \d : liste des relations
-- \dt : liste des tables
-- \? : aide

-- Mdp postgres = 1234
-- Pour changer le mot de passe : ALTER USER postgres WITH ENCRYPTED PASSWORD '1234';

------------------------------------------
-- TABLES
------------------------------------------

drop table if exists commentaire cascade;

drop table if exists achat cascade;
drop table if exists location cascade;
drop table if exists infoCB cascade;

drop table if exists utilisateur cascade;

drop table if exists info_serie cascade;
drop table if exists statistiques_video cascade;
drop table if exists video cascade;

------------------------------------------
-- UTILISATEURS
------------------------------------------
create table utilisateur(
	id_utilisateur serial primary key,
	nom varchar(255) not null,
	prenom varchar(255) not null,
	adresse varchar(255) not null,
	telephone bigint,
	login varchar(255) not null unique,
	mdp varchar(255) not null,
	type varchar(255) not null,
	check (type='Premium' or type='Non premium' or type='Administrateur')
);

-- REMPLISSAGE UTILISATEUR : mdp = 123456
insert into utilisateur(nom, prenom, adresse, telephone, login, mdp, type) values ('prem', 'toto', 'prem.toto@netflox.com', 0101010101, 'premtoto', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Premium');
insert into utilisateur(nom, prenom, adresse, telephone, login, mdp, type) values ('nonprem', 'toto', 'nonprem.toto@netflox.com', 0101010101, 'nonpremtoto', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Non premium');
insert into utilisateur(nom, prenom, adresse, telephone, login, mdp, type) values ('root', 'toto', 'root.toto@netflox.com', 0101010101, 'roottoto', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Administrateur');

------------------------------------------
-- CB
------------------------------------------
create table infoCB(
	id_utilisateur int not null,
	foreign key (id_utilisateur) references utilisateur(id_utilisateur),
	-- num_cb int primary key,
	num_cb bigint primary key,
	nom_cb varchar(255),
	type_cb varchar(255) not null,
	check (type_cb ='Visa' or type_cb='American Express' or type_cb='Master Card'),
	date_ex timestamp not null,
	cryptogramme int
);	

------------------------------------------
-- VIDEOS
------------------------------------------
create table video(
	id_video serial primary key,
	titre varchar(255) not null,
	auteur varchar(255) not null,
	annee int not null,
	prix int not null,
	check (prix > 0 and prix < 250),
	etat varchar(255) not null,
	check (etat='Disponible' or etat='Non disponible'),
	categorie varchar(255) not null,
	genre varchar(255) not null,
	check (((categorie='Serie' or categorie='Film') and (genre ='Policier' or genre='Action' or genre ='Romantique' or genre ='Sc-Fiction')
		) or (categorie='Documentaire' and (genre ='Biographie' or genre ='Animalier' or genre='Nature' or genre='Styledevie'))),
	motscles varchar(255) not null,
	nom_img varchar(255) not null,
	synopsis text,
	nom_video varchar(255)
);

-- REMPLISSAGE TABLE VIDEO A PARTIR DU FICHIER CSV
\copy video (titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video) FROM 'C:\Users\sophie\Bitbucket\projet-netflox\code\WebContent\resources\videos.csv' DELIMITER ';' csv;
\copy video (titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video) FROM 'C:\Users\Miary\Documents\projet-netflox\code\WebContent\resources\videos.csv' DELIMITER ';' csv;
\copy video (titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video) FROM 'C:\Users\Lanson\Documents\netflox\code\WebContent\resources\videos.csv' DELIMITER ';' csv;
\copy video (titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video) FROM 'C:\Users\Tongasoa\Bitbucket\projet-netflox\code\WebContent\resources\videos.csv' DELIMITER ';' csv;

create table info_serie(
	id_video int,
	foreign key (id_video) references video(id_video),
	saison int not null,
	episode int not null
);

-- REMPLISSAGE TABLE INFO_SERIE A PARTIR DU FICHIER CSV
\copy info_serie (id_video, saison, episode) FROM 'C:\Users\sophie\Bitbucket\projet-netflox\code\WebContent\resources\series.csv' DELIMITER ';' csv;
\copy info_serie (id_video, saison, episode) FROM 'C:\Users\Miary\Documents\projet-netflox\code\WebContent\resources\series.csv' DELIMITER ';' csv;
\copy info_serie (id_video, saison, episode) FROM 'C:\Users\Lanson\Documents\netflox\code\WebContent\resources\series.csv' DELIMITER ';' csv;
\copy info_serie (id_video, saison, episode)  FROM 'C:\Users\Tongasoa\Bitbucket\projet-netflox\code\WebContent\resources\series.csv ' DELIMITER ';' csv;

create table statistiques_video(
	id_video int primary key,
	foreign key (id_video) references video(id_video),
	nb_vue int default 0,
	nb_telechargement int default 0
);

-- REMPLISSAGE TABLE STATISTIQUES_VIDEO A PARTIR DU FICHIER CSV
\copy statistiques_video (id_video, nb_vue, nb_telechargement) FROM 'C:\Users\sophie\Bitbucket\projet-netflox\code\WebContent\resources\statistiques_video.csv' DELIMITER ';' csv;
\copy statistiques_video (id_video, nb_vue, nb_telechargement) FROM 'C:\Users\Miary\Documents\projet-netflox\code\WebContent\resources\statistiques_video.csv' DELIMITER ';' csv;
\copy statistiques_video (id_video, nb_vue, nb_telechargement) FROM 'C:\Users\Lanson\Documents\netflox\code\WebContent\resources\statistiques_video.csv' DELIMITER ';' csv;
\copy statistiques_video (id_video, nb_vue, nb_telechargement) FROM 'C:\Users\Tongasoa\Bitbucket\projet-netflox\code\WebContent\resources\statistiques_video.csv' DELIMITER ';' csv;

------------------------------------------
-- ACTIONS
------------------------------------------
create table achat(
	id_achat serial primary key,
	id_utilisateur int,
	foreign key (id_utilisateur) references utilisateur(id_utilisateur),
	id_video int,
	foreign key (id_video) references video(id_video),
	date_achat timestamp not null
);

create table location(
	id_loc serial primary key,
	id_utilisateur int,
	foreign key (id_utilisateur) references utilisateur(id_utilisateur),
	id_video int,
	foreign key (id_video) references video(id_video),
	date_loc timestamp not null
);

------------------------------------------
-- FONCTIONNALITE SURPRISE : COMMENTAIRE
------------------------------------------

create table commentaire(
	id_commentaire serial primary key,
	id_video int,
	foreign key (id_video) references video(id_video),
	id_utilisateur int,
	foreign key (id_utilisateur) references utilisateur(id_utilisateur),
	titre_commentaire varchar(255) not null,
	nom_utilisateur varchar(255) not null,
	prenom_utilisateur varchar(255) not null,
	texte text,
	date_commentaire timestamp not null
);