<!-- footer -->

<!-- Pied de page que nous avons inclus dans les autres pages avec la fonction include -->
<!-- auteur : Miary -->
<div class="footer">
	<div class="footer-grids">
		<div class="footer-top">
			<div class="footer-top-nav">
				<ul>
					<li><a href="">A propos</a></li>
					<li><a href="">Copyright</a></li>
					<li><a href="">Designers</a></li>
					<li><a href="">Publicités</a></li>
					<li><a href="">Développeurs</a></li>
				</ul>
			</div>
			<div class="footer-bottom-nav">
				<ul>
					<li><a href="">Termes et conditions</a></li>
						<li><a href="">Sécurité</a></li>
						<li><a href="#small-dialog4"
							class="">Envoyer un feedback</a></li>
				</ul>
			</div>
		</div>
		<!--
			<div class="footer-bottom">
				<ul>
					<li><a href="#small-dialog5"
						class="play-icon popup-with-zoom-anim f-history f-help">Help</a></li>
				</ul>
			</div>
			-->
	</div>
</div>
<!-- //footer -->