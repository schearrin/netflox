<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- 			L'administrateur choisit le genre de vid�o qu'il veut uploader -->
<!-- 			auteur : Miary -->

<c:set var="IDu" value="${IDu}" />
<c:set var="TYPEu" value="${TYPEu}" />
<c:set var="login" value="${login}" />
<c:set var="PRENOMu" value="${PRENOMu}" />
<c:set var="NOMu" value="${NOMu}" />


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="main-grids news-main-grids">
			<div class="recommended-info">
				<c:if test="${TYPEu =='Administrateur'}"><h1>Panneau d'administration</h1></c:if>
				<c:if test="${TYPEu !='Administrateur'}"><h1>Mon compte client</h1></c:if>
			</div>
			<h3>Bonjour <c:out value="${PRENOMu}" /> <c:out value="${NOMu}" /> !</h3>
			<h4> - votre identifiant est : <c:out value="${IDu}" /></h4>
			<h4> - votre login est : <c:out value="${login}" /></h4>
			<h4> - votre niveau de compte : <c:out value="${TYPEu}" /></h4>
			<br>
			<div class="clearfix"></div>
			
			<c:if test="${TYPEu =='Administrateur'}">
			<h3>Actions sur vid�o</h3>
			<h4>- ajouter un <a href="admin_adddoc.jsp">documentaire</a></h4>
			<h4>- ajouter un <a href="admin_addfilm.jsp">film</a></h4>
			<h4>- ajouter une <a href="admin_addserie.jsp">s�rie</a></h4>
			<br>
			<h4>- supprimer une <a href="supVideo.jsp">vid�o</a></h4>

			<div class="clearfix"></div>

			<h3>Actions sur client</h3>
			<h4>- ajouter un <a href="addClt.jsp">client</a></h4>
			<h4>- modifier un <a href="modClt.jsp">client</a></h4>
			<h4>- supprimer un <a href="supClt.jsp">client</a></h4>

			<div class="clearfix"></div>
			
			<h3>PDF Catalogue et Audit</h3>
			<h4>- g�n�rer les <a href="PDF">PDFs</a></h4>

			<div class="clearfix"></div>
			
			<h3>Actions sur commentaire</h3>
			<h4>- pour supprimer un commentaire, rendez-vous sur la page de
				la vid�o en question et cliquer sur [X] pour supprimer le
				commentaire</h4>

			<div class="clearfix"></div>
			</c:if>
			
			<c:if test="${TYPEu !='Administrateur'}">
			<c:if test="${TYPEu !='Premium'}">
			<h3>Louer une vid�o</h3>
			<h4>- pour louer une vid�o, rendez-vous sur la page de la vid�o et cliquer sur "Louer"</h4>
			<div class="clearfix"></div>
			</c:if>
			<h3>Acheter une vid�o</h3>
			<h4>- pour acheter une vid�o, rendez-vous sur la page de la vid�o et cliquer sur "Acheter"</h4>
			<div class="clearfix"></div>
			<h3>Poster un commentaire</h3>
			<h4>- pour poster un commentaire, rendez-vous sur la page de la vid�o puis remplisser le formulaire pour ajouter un commentaire</h4>
			<div class="clearfix"></div>
			</c:if>
		</div>

	</div>
</div>
</body>
</html>