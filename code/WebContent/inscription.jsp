<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<div class="recommended-info"></div>
<!-- 			Pour se connecter avec les r�seaux sociaux -->
<!-- 			(Cette fonctionnalit� ne marche pas, c'est juste pour l'apparence) -->
<!-- 			auteur : Miary -->
			<h3>Cr�er un compte</h3>
			<div class="social-sits">
				<div class="facebook-button">
					<a href="#">Se connecter avec Facebook</a>
				</div>
				<div class="chrome-button">
					<a href="#">Se connecter avec Google</a>
				</div>
				<div class="button-bottom">
					<p>
						Nouveau client? <a href="inscription.jsp" >Inscrivez-vous</a>
					</p>
				</div>
			</div>
<!-- 			Formulaire d'inscription � remplir par le client -->
<!-- 			auteur : Miary -->
			</div>
			<div class="signup">
				<form action="inscription.jsp" method=post>
					<input type="text" 
						class="email" 
						placeholder="Nom"
						name = "nom"
						required="required" 
						title="Entrez votre nom" /> 
					<input type="text" 
						class="email" 
						placeholder="Pr�nom" 
						name = "prenom"
						required="required"
						title="Entrez votre pr�nom" /> 
					<input type="text" 
						class="email"
						placeholder="E-mail" 
						name = adresse
						required="required"
						pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?"
						title="Entrez une adresse e-mail valide" /> 
					<input type="text"
						class="email" 
						placeholder="Num�ro de t�l�phone" 
						name = "telephone"
						maxlength="10"
						pattern="[0-9]{1}\d{9}"
						title="Entrez un num�ro de t�l�phone valide" /> 
					<input
						type="text" 
						class="email" 
						placeholder="Login" 
						name = "login"
						required="required"
						title="Entrez votre login" /> 
					<input type="password"
						placeholder="Mot de passe" 
						name = "mdp"
						required="required" 
						pattern=".{6,}"
						title="Longueur minimum : 6 caract�res" 
						autocomplete="off" /> 
					<input type="submit" 
						value="Je m'inscris" />
						
				</form>
			</div>
			<div class="clearfix"></div>

			<script src="js/bootstrap.min.js"></script>
			</div>
			</div>
			</body>
			</html>