<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="res_affcommentaire" value="${res_affcommentaire}" />

<div class="all-comments">
	<!-- Debut formulaire pour poster un commentaire -->
	<div class="all-comments-info">
		<a href="#">Tous les commentaires</a>
		<c:if test="${! empty TYPEu}">
		<div class="box">
			<form action="commentaire_ok" method="post">
				<input type="text" class="email" placeholder="Titre du commentaire" name="titre_commentaire"
					required="required" />
				<textarea placeholder="Message" name="texte_commentaire"></textarea>
				<input type="submit" value="Commenter">
				<div class="clearfix"></div>
			</form>
		</div>
		</c:if>
	</div>
	<!-- Fin formulaire pour poster un commentaire -->


	<!-- Debut affichage des commentaires -->
	<div class="media-grids">
		<c:choose>
			<c:when test="${! empty res_affcommentaire}">
				<c:forEach var="com" items="${res_affcommentaire}">
					<div class="media">
						<h5><c:out value="${com.value['titre_commentaire']}" /> 
						<c:if test="${TYPEu == 'Administrateur'}">
						- <a href="admin_ok_delcomm?id_commentaire=${com.key}">[X]</a>
						</c:if>
						</h5>
						<div class="media-left">
							<a href="#"> </a>
						</div>
						<div class="media-body">
							<p><c:out value="${com.value['texte']}" /></p>
							<span>Commentaire post� par :<a href="#"> <c:out value="${com.value['prenom_utilisateur']}" /> <c:out value="${com.value['nom_utilisateur']}" /> </a></span>
							<span>le :<a href="#"> <c:out value="${com.value['date_commentaire']}" />.</a></span>
						</div>
					</div>
				</c:forEach>
			</c:when>
		</c:choose>
	</div>
	<!-- Fin affichage des commentaires -->

</div>