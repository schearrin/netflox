<link href="css/popuo-box.css" rel="stylesheet" type="text/css"
	media="all" />
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- 
	supClt.jsp
	@author lanson 
	Formulaire pour supprimer un client
	 -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="show-top-grids">
		<div class="col-sm-10 show-grid-left main-grids">
			<div class="recommended">
				<div class="recommended-grids english-grid">
					<div class="recommended-info">
					<h2>Pour supprimer un client : </h2>
					</div>
						<div class="signup">
							<form action="sclient" method=post>
								<input type="text" class="email"
									placeholder="Entrez l'identifiant du client � supprimer"
									name="id_utilisateur" required="required"
									title="Entrez l'identifiant du client" /> <input type="submit"
									value="Supprimer" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>