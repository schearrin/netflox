<link href="css/popuo-box.css" rel="stylesheet" type="text/css"
	media="all" />
<%@include file="entete.jsp"%>
<%@include file="menu.jsp"%>

<!-- 
	addClt.jsp
	@author miary, lanson 
	Formulaire pour ajouter un client
 -->

<c:set var="TYPEu" value="${TYPEu}" />

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="main-grids">
		<div class="top-grids">
			<div class="recommended-info"></div>
			<h3>Cr�er un compte</h3>
		</div>
		<!-- 	Formulaire � remplir par l'internaute pour qu'il puisse s'inscrire -->
		<div class="signup">
			<form action="client" method="post">
				<input type="text" class="email" placeholder="Nom" name="nom"
					required="required" title="Entrez votre nom" /> <input type="text"
					class="email" placeholder="Pr�nom" name="prenom"
					required="required" title="Entrez votre pr�nom" /> <input
					type="text" class="email" placeholder="adresse" name=adresse
					required="required" pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?"
					title="Entrez une adresse e-mail valide" /> <input type="text"
					required="required" class="email" placeholder="Num�ro de t�l�phone"
					name="telephone" maxlength="10" pattern="[0-9]{1}\d{9}"
					title="Entrez un num�ro de t�l�phone valide" /> <input type="text"
					class="email" placeholder="Login" name="login" required="required"
					title="Entrez votre login" /> <input type="password"
					placeholder="Mot de passe" name="mdp" required="required"
					pattern=".{6,}" title="Longueur minimum : 6 caract�res"
					autocomplete="off" />
				<h4>
					<c:if test="${!empty TYPEu}">
					<label for="Prem">Ajout d'un client Premium ou Non premium ?</label>
					</c:if>
					<c:if test="${empty TYPEu}">
					<label for="Prem">Voulez-vous �tre Premium ?</label>
					</c:if>
				</h4>
				<br>

				<!-- Debut rajout d'un script pour formulaire de paiement -->
				<!--
					script permettant d'afficher le formulaire de paiement apres selection de Premium
					@author : sophie
				 -->
				<script type="text/javascript">
					function montreFormulairePaiement(valeur) {
						if (valeur == "Premium") {
							document.getElementById("id_formpaiement").style.display = 'block';
						}
						if (valeur == "Non premium") {
							document.getElementById("id_formpaiement").style.display = 'none';
						}
						if (valeur == "Administrateur") {
							document.getElementById("id_formpaiement").style.display = 'none';
						}
					}
				</script>
				<!-- Fin rajout d'un script pour formulaire de paiement -->


				<select style="width: 90%; padding: 10px; font-size: 14px;"
					onchange="montreFormulairePaiement(this.value)" name="type"
					id="type">
					<option selected="selected" value="Premium">Premium</option>
					<option value="Non premium">Non premium</option>
					<c:if test="${TYPEu =='Administrateur'}">
						<option value="Administrateur">Administrateur</option>
					</c:if>
				</select> <br> <br> <br>

				<!-- Debut formulaire de paiement apres selection de Premium -->
				<div id="id_formpaiement">
					<h4>
						<label for="Paiement">Veuillez aussi effectuer votre
							paiement pour �tre premium :</label>
					</h4>

					<h4>S�lectionnez votre type de CB :</h4>
					<select style="width: 90%; padding: 10px; font-size: 14px;"
						class="email" name="typeCB" id="typeCB">
						<option value="visa">Visa</option>
						<option value="american_express">American Express</option>
						<option value="master_card">Master Card</option>
					</select> <br>
					<h4>Num�ro de votre CB :</h4>
					<input type="text" class="email" placeholder="NumCB" name="numCB"
						title="Entrez le num�ro de votre CB" /> <br>
					<h4>Cryptogramme :</h4>
					<input type="text" class="email" placeholder="crytoCB"
						name="cryptoCB" title="Entrez le num�ro du cryptogramme" /> <br>
					<h4>Date d'expiration:</h4>
					<input type="text" class="email" placeholder="mois" name="mois"
						title="Entrez le mois" /> <input type="text" class="email"
						placeholder="annee" name="annee"
						title="Entrez l'ann�e" /> <br> <br>
				</div>
				<!-- Fin formulaire de paiement apres selection de Premium -->

				<input type="submit" value="Je m'inscris" /> <br>
			</form>
			<br>
		</div>
		<div class="clearfix"></div>
		<script src="js/bootstrap.min.js"></script>
	</div>
</div>