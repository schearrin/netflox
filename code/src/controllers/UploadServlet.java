package controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Controleur pour faire un upload
 * @author miary, sophie
 */
public class UploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	//private final String UPLOAD_DIRECTORY = "C:/Users/Miary/Documents/projet-netflox/code/WebContent/video";
	private final String UPLOAD_DIRECTORY = "C:/Users/sophie/Bitbucket/projet-netflox/code/WebContent/video";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Lanson/Documents/netflox/code/WebContent/video";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Tongasoa/Bitbucket/projet-netflox/code/WebContent/video";
	
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

    	//Si multipart content
    	if(ServletFileUpload.isMultipartContent(request)){
    		try {
    			//Releve tout les items de la requete dans une liste
    			List<FileItem> multiparts = new ServletFileUpload(
    					new DiskFileItemFactory()).parseRequest(request);

    			for(FileItem item : multiparts){
    				//Si c'est le type est un fichier et non type dans formulaire
    				if(!item.isFormField()){

    					//Expression regulieres qui commence par caratere non blanc, finissent par ces extentions et insensible a la casse
    					String regex_img = "([^\\s]+(\\.(?i)(bmp|jpg|gif|png))$)";
    					String regex_video = "([^\\s]+(\\.(?i)(mp4|avi|wmv|mkv))$)";

    					String name = new File(item.getName()).getName();
    					System.out.println(name);
    					
    					//Si le pattern image est correct alors :
    					if(Pattern.matches(regex_img, name)){
    						item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
    						System.out.println("image upload ok");
    					}else{System.out.println("image upload pas ok");}
    					
    					//Si le pattern video est correct alors :
    					if(Pattern.matches(regex_video, name)){
    						item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
    						System.out.println("video upload ok");
    					}else{System.out.println("video upload pas ok");}
    				}
    			}

                //File uploaded successfully
               request.setAttribute("message", "La video a bien �t� upload�.");
            } catch (Exception ex) {
               request.setAttribute("message", "File Upload Failed due to " + ex);
            }          
         
        }else{
            request.setAttribute("message",
                                 "Sorry this Servlet only handles file upload request");
        }
    
        request.getRequestDispatcher("/result.jsp").forward(request, response);
    }
  
}