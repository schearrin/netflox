package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.ModCltDB;
/**
 * Classe modcltServlet
 * @author lanson
 * modifier le client (controleur)
 */

@WebServlet("/modcltServlet")
public class modcltServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public modcltServlet() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//R�cup�ration des param�tres par le serveur
		String ID = request.getParameter("id_utilisateur");
		String lname = request.getParameter("nom");
		String fname = request.getParameter("prenom");
		String adr = request.getParameter("adresse");
		String tel = request.getParameter("telephone");
		String log = request.getParameter("login");
		String passwd = request.getParameter("mdp");
		String type = request.getParameter("type");

		getServletContext().log("Dans modcltServlet.doPost");
		
		int te = Integer.parseInt(tel);
		int id = Integer.parseInt(ID);
		
		try {
			//Recupere connexion
			ModCltDB db2 = (ModCltDB) request.getSession().getAttribute("db2");
			if (db2 == null) {
				ServletContext ctx = this.getServletContext();
				db2 = new ModCltDB(ctx);
				request.getSession().setAttribute("db2", db2);
			}

			//definit les attributs
			request.setAttribute("verifID", db2.checkID(id));
			boolean verifID = db2.checkID(id);
			
			if (verifID){
				request.setAttribute("msg", "Le client a bien �t� modifi�.");
				request.setAttribute("res_clt", db2.modUser(id, lname,fname,adr, te, log, passwd, type ));
			}else{
				request.setAttribute("msg", "l'identifiant n'existe pas !");
			}
			
			RequestDispatcher rd = request.getRequestDispatcher("/mclient.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			System.out.println("Erreur userServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
}
