package controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.ActionClientDB;
import models.CommentaireDB;
import models.ConsultationVideoDB;
import models.StatistiquesDB;
import models.SuggestionVideoDB;

/**
 * Controleur qui affiche le detail d'une video
 * @author sophie
 *
 */
@WebServlet("/VideoDetailServlet")
public class VideoDetailServlet extends HttpServlet {

	private static final long serialVersionUID = 689492429435860648L;

	public VideoDetailServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Recupere le parametre id
		String param_id = request.getParameter("id");

		int id = -1;
		try {
			id = Integer.parseInt(param_id);
		} catch (NumberFormatException e) {
		}

		try{
			//1) consultation
			ConsultationVideoDB db_vdetail = (ConsultationVideoDB) request.getSession().getAttribute("db_vdetail");
			if (db_vdetail == null) {

				db_vdetail = new ConsultationVideoDB();
				request.getSession().setAttribute("db_vdetail", db_vdetail);

			}

			//Definit les attributs
			request.setAttribute("id", new Integer(param_id));
			request.setAttribute("res_detailvideo", db_vdetail.getVideoDetail(id));
			
			//Sauvegarde id video dans session
			HttpSession session = request.getSession();
			session.setAttribute("sessionIDV", param_id);
			
			//2) statistiques
			StatistiquesDB db_vstat = (StatistiquesDB) request.getSession().getAttribute("db_vstat");
			if (db_vstat == null) {

				db_vstat = new StatistiquesDB();
				request.getSession().setAttribute("db_vstat", db_vstat);

			}

			//Incremente le nombre de vues et l'affiche
			db_vstat.incrementNbVue(id);
			request.setAttribute("nbVueVideo", db_vstat.getNbVueVideo(id));
			
			//Retourne le nombre de tÚlÚchargement
			request.setAttribute("nbTelechargementVideo", db_vstat.getNbTelechargementVideo(id));
			
			
			//3) suggestion
			SuggestionVideoDB db_vsug = (SuggestionVideoDB) request.getSession().getAttribute("db_vsug");
			if (db_vsug == null) {

				db_vsug = new SuggestionVideoDB();
				request.getSession().setAttribute("db_vsug", db_vsug);

			}
			//Definit les attributs
			request.setAttribute("res_suggestionvideo", db_vsug.suggestionVideo(id));
			
			//4) Action des clients
			ActionClientDB db_ac =(ActionClientDB) request.getSession().getAttribute("db_ac");
			if (db_ac == null) {

				db_ac = new ActionClientDB();
				request.getSession().setAttribute("db_ac", db_ac);

			}
			//Recupere l'id utilisateur de la session en cours

			String param_idu = (String) session.getAttribute("IDu");
			int idu = 0;
			try {
				idu = Integer.parseInt(param_idu);
			} catch (NumberFormatException e) {
			}
			//Definit les attributs
			request.setAttribute("si_locationexiste", db_ac.siLocationExisteDeja(idu, id));
			
			//Met a jour la liste des locations et supprime toutes celles qui depasse 10h
			db_ac.MAJ_deleteLocation();

			//5) Affichage des commentaires
			CommentaireDB db_vcom = (CommentaireDB) request.getSession().getAttribute("db_vcom");
			if (db_vcom == null) {

				db_vcom = new CommentaireDB();
				request.getSession().setAttribute("db_vcom", db_vcom);

			}
			
			request.setAttribute("res_affcommentaire", db_vcom.AfficheCommentaire(id));		
			
			//Redirection vers la page jsp
			RequestDispatcher rd = request.getRequestDispatcher("/video.jsp");;
			rd.forward(request, response);
			
			System.out.println("Ok VideoDetailServlet");

		}catch(Exception e){
			System.out.println("Erreur VideoDetailServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
