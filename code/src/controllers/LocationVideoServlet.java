package controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.ActionClientDB;

/**
 * Controleur pour la location d'une vid�o
 * @author sophie
 *
 */
@WebServlet("/LocationVideoServlet")
public class LocationVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LocationVideoServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		HttpSession session = request.getSession();

		String param_idv = (String) session.getAttribute("sessionIDV");
		int idv = Integer.parseInt(param_idv);
		System.out.println(idv);
		String param_idu = (String) session.getAttribute("IDu");
		int idu = Integer.parseInt(param_idu);
		System.out.println(idu);

		java.util.Date date= new java.util.Date();
		Timestamp current_time = new Timestamp(date.getTime());

		try{
			//Recupere connexion
			ActionClientDB db_aclt = (ActionClientDB) request.getSession().getAttribute("db_aclt");
			if (db_aclt == null) {

				db_aclt = new ActionClientDB();
				request.getSession().setAttribute("db_aclt", db_aclt);

			}

			//Definit les attributs
			request.setAttribute("res_location", db_aclt.paiementAchatLocation("Location", idu, idv, current_time));

			//Redirection vers la page jsp
			RequestDispatcher rd = request.getRequestDispatcher("/client_resplocation.jsp");;
			rd.forward(request, response);
			
			System.out.println("Ok LocationVideoServlet : Get");

		} catch (Exception e) {
			
			System.out.println("Erreur LocationVideoServlet : Get");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Recupere les parametres du formulaire
		String param_typeCB = request.getParameter("typeCB");
		String param_numCB = request.getParameter("numCB");
		String param_cryptoCB = request.getParameter("cryptoCB");
		String param_mois = request.getParameter("mois");
		String param_annee = request.getParameter("annee");

//		int numCB = 0;
		long numCB = 0;
		int cryptoCB = 0;
		int mois = 0;
		int annee = 0;
		try {
//			numCB = Integer.parseInt(param_numCB);
			numCB = Long.parseLong(param_numCB);
			cryptoCB = Integer.parseInt(param_cryptoCB);
			mois = Integer.parseInt(param_mois);
			annee = Integer.parseInt(param_annee);
		} catch (NumberFormatException e) {System.out.println("problem : number format exception");}


		try {
			//Recupere connexion
			ActionClientDB db_aclt = (ActionClientDB) request.getSession().getAttribute("db_aclt");
			if (db_aclt == null) {

				db_aclt = new ActionClientDB();
				request.getSession().setAttribute("db_aclt", db_aclt);

			}

			request.setAttribute("verif", db_aclt.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee));
			boolean verif = db_aclt.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee);

			if(verif){
				doGet(request, response);

			}else{
				//Redirection vers la page jsp avec message erreur
				request.setAttribute("erreur_paiement", "Erreur paiement, veuillez verifier vos informations.");

				RequestDispatcher rd = request.getRequestDispatcher("/client_resplocation.jsp");;
				rd.forward(request, response);
			}

			System.out.println("Ok LocationVideoServlet : Post");

		} catch (Exception e) {
			System.out.println("Erreur LocationVideoServlet : Post");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

}
