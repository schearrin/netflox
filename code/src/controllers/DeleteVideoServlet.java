
package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.ActionVideoDB;

/**
 * Controleur pour la suppression d'une vid�o
 * @author miary, sophie
 *
 */
@WebServlet("/DeleteVideoServlet")
public class DeleteVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteVideoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Recupere le parametre id
		String param_id = request.getParameter("id");
		int id = Integer.parseInt(param_id);
		
		try{
			//Recupere connexion
			ActionVideoDB db_vdelete = (ActionVideoDB) request.getSession().getAttribute("db_vdelete");
			if (db_vdelete == null) {

				db_vdelete = new ActionVideoDB();
				request.getSession().setAttribute("db_vdelete", db_vdelete);

			}
			
			//Definit les attributs
			request.setAttribute("res_delvideo",db_vdelete.deleteVideo(id));
			
			//Redirection vers page jsp
			RequestDispatcher rd = request.getRequestDispatcher("/svideo.jsp");;
			rd.forward(request, response);

		}catch(Exception e){
			System.out.println("Erreur DeleteVideoServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

}
