package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.SupCltDB;

/**
 * Classe supcltServlet
 * @author lanson
 *supprimer un client(controleur)
 */

@WebServlet("/supcltServlet")
public class supcltServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	public supcltServlet() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Récupération des paramètres par le serveur

		String ID = request.getParameter("id_utilisateur");
		
		getServletContext().log("Dans supcltServlet.doPost");
		
		
			 int id = Integer.parseInt(ID);
		
		
		


		try {
			//Recupere connexion
			SupCltDB db = (SupCltDB) request.getSession().getAttribute("db");
			if (db == null) {
				ServletContext ctx = this.getServletContext();
				db = new SupCltDB(ctx);
				request.getSession().setAttribute("db", db);

			}
			//definit les attributs
			request.setAttribute("verifID", db.checkID(id));
			boolean verifID = db.checkID(id);
		if (verifID){	
			request.setAttribute("msg", "vous avez bien supprimé le client !");
			request.setAttribute("res_clt", db.supUser(id));
		}else{
			request.setAttribute("msg", "l'identifiant n'existe pas !");
		}
		
		
			RequestDispatcher rd = request.getRequestDispatcher("/sclient.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			System.out.println("Erreur userServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}

	}

}

