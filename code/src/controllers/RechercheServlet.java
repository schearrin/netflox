package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.SearchVideoDB;

/**
 * Controleur pour la recherche d'une vid�o
 * @author sophie
 *
 */

@WebServlet("/RechercheServlet")
public class RechercheServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RechercheServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Recupere le parametre Recherche du formulaire de recherche
		String param_recherche = request.getParameter("Recherche");

		//Verifie qu'elle n'est pas vide
		if(!param_recherche.equals("")){
			try{
				//Recupere connexion
				SearchVideoDB db_vsearch = (SearchVideoDB) request.getSession().getAttribute("db_vsearch");
				if (db_vsearch == null) {

					db_vsearch = new SearchVideoDB();
					request.getSession().setAttribute("db_vsearch", db_vsearch);

				}
								
				request.setAttribute("param_recherche", new String(param_recherche));
				request.setAttribute("res_recherche", db_vsearch.searchVideo(param_recherche));
				
				
				//test
			/*	System.out.println("affiche element tableau depuis servlet");
				HashMap<Integer, Video> hv = db_vsearch.searchVideo2(param_recherche);
				for(Entry<Integer, Video> entry : hv.entrySet()) {
				    Integer key = entry.getKey();
				    Video value = entry.getValue();
				    System.out.println(key+" : "+value);
				} 
				System.out.println("affiche element tableau depuis servlet fin");
				*/
				
				

				//Redirection vers page jsp
				RequestDispatcher rd = request.getRequestDispatcher("/search.jsp");;
				rd.forward(request, response);
				
				System.out.println("Ok RechercheServlet");

			}catch(Exception e){
				System.out.println("Erreur RechercheServlet");
				e.printStackTrace();
				throw new ServletException(e);
			}
		}else{
			//Si vide, renvoie vers page de recherche vide
			RequestDispatcher rd = request.getRequestDispatcher("/search.jsp");;
			rd.forward(request, response);
		}

		doGet(request, response);
	}


}
