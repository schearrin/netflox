package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.ConnexionFormDB;

/**
 * Controleur pour la connexion
 * @author tong
 */

@WebServlet("/ConnexionServlet")
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public ConnexionServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Récupération des paramètres par le serveur
		String log = request.getParameter("login");
		String passwd = request.getParameter("mdp");


		//Deconnexion de toute session precedente
		HttpSession session = request.getSession();
		if(request.getSession() != null){
			session.invalidate();
		}
		try{
			//Recupere connexion
			ConnexionFormDB db_connex = (ConnexionFormDB) request.getSession().getAttribute("db_connex");
			if (db_connex == null) {
				db_connex = new ConnexionFormDB();
				request.getSession().setAttribute("db_connex", db_connex);
			}


			boolean res_connexion = db_connex.connexionUtilisateur( log,  passwd );

			if(res_connexion){

				//Recupere une session et definit les attributs de session
				session = request.getSession(true);
				session.setAttribute("IDu", db_connex.getIdUtilisateur(log));
				session.setAttribute("TYPEu", db_connex.getTypeUtilisateur(log));
				session.setAttribute("PRENOMu", db_connex.getPrenomUtilisateur(log));
				session.setAttribute("NOMu", db_connex.getNomUtilisateur(log));

				session.setAttribute("login", log );
				session.setAttribute("mdp", passwd );

				//this.getServletContext().getRequestDispatcher("/connexion.jsp").forward(request, response);

			}else{
				//session.invalidate();
				request.setAttribute("msg", "Le nom d'utilisateur ou le mot de passe n'est pas valide.");

			}	    

			RequestDispatcher rd = request.getRequestDispatcher("/connexion.jsp");;
			rd.forward(request, response);

			System.out.println("Ok ConnexionServlet");

		}catch(Exception e){
			System.out.println("Erreur ConnexionServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
		doGet(request, response);
	}

}
