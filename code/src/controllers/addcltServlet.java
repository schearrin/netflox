package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.ActionClientDB;
import models.AddCltDB;

/**
 * Classe addcltServlet
 * @author lanson
 * ajouter client (controleur)
 */


@WebServlet("/addcltServlet")
public class addcltServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	public addcltServlet() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//R�cup�ration des param�tres par le serveur

		String lname = request.getParameter("nom");
		String fname = request.getParameter("prenom");
		String adr = request.getParameter("adresse");
		String tel = request.getParameter("telephone");
		String log = request.getParameter("login");
		String passwd = request.getParameter("mdp");
		String type = request.getParameter("type");
		getServletContext().log("Dans addcltServlet.doPost");

		int te = Integer.parseInt(tel);

		try {
			//Recupere connexion
			AddCltDB db1 = (AddCltDB) request.getSession().getAttribute("db1");
			if (db1 == null) {
				ServletContext ctx = this.getServletContext();
				db1 = new AddCltDB(ctx);
				request.getSession().setAttribute("db1", db1);

			}

			//Si le client n'est pas premium
			if(type.equals("Non premium")){
				request.setAttribute("msg", "Vous avez bien ajout� le client "+ lname +" "+ fname);
				request.setAttribute("res_clt", db1.addUser(lname,fname,adr, te, log, passwd, type ));

				RequestDispatcher rd = request.getRequestDispatcher("/client.jsp");
				rd.forward(request, response);
			}
			
			//Si le client veut etre premium
			if(type.equals("Premium")){
				
				//Recupere connexion sur ActionClientDB
				ActionClientDB db_verp = (ActionClientDB) request.getSession().getAttribute("db_verp");
				if (db_verp == null) {
					db_verp = new ActionClientDB();
					request.getSession().setAttribute("db_verp", db_verp);

				}
				
				//Recupere les parametres concernant le paiement
				String param_typeCB = request.getParameter("typeCB");
				String param_numCB = request.getParameter("numCB");
				String param_cryptoCB = request.getParameter("cryptoCB");
				String param_mois = request.getParameter("mois");
				String param_annee = request.getParameter("annee");
				
				System.out.println(param_typeCB+" "+param_numCB);
//				int numCB = 0;
				long numCB = 0;
				int cryptoCB = 0;
				int mois = 0;
				int annee = 0;
				try {
//					numCB = Integer.parseInt(param_numCB);
					numCB = Long.parseLong(param_numCB);
					cryptoCB = Integer.parseInt(param_cryptoCB);
					mois = Integer.parseInt(param_mois);
					annee = Integer.parseInt(param_annee);
				} catch (NumberFormatException e) {System.out.println("problem : number format exception");}
				
				request.setAttribute("si_verifpaiement", db_verp.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee));
				boolean si_verifpaiement = db_verp.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee);
				
				System.out.println(si_verifpaiement);
				
				//si la verification du paiement est valide
				if(si_verifpaiement){
					
					request.setAttribute("msg", "Le client a bien �t� inscrit "+ lname +" "+ fname);
					request.setAttribute("res_clt", db1.addUser(lname,fname,adr, te, log, passwd, type ));

				}else{ //sinon
					request.setAttribute("msg", "Le paiement effectu� n'est pas valide");
				}

				RequestDispatcher rd = request.getRequestDispatcher("/client.jsp");
				rd.forward(request, response);
			}

		} catch (Exception e) {
			System.out.println("Erreur userServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}

	}

}

