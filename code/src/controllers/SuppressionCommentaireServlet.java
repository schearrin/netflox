package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.CommentaireDB;

/**
 * Controleur pour la suppresion d'un commentaire
 * @author miary, sophie
 */
public class SuppressionCommentaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SuppressionCommentaireServlet() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//R�cup�ration des param�tres par le serveur

		String ID = request.getParameter("id_commentaire");	
		
			 int id = Integer.parseInt(ID);
		
		try {
			//Recupere connexion
			CommentaireDB db_commentaire = (CommentaireDB) request.getSession().getAttribute("db_commentaire");
			if (db_commentaire == null) {
				db_commentaire = new CommentaireDB();
				request.getSession().setAttribute("db_commentaire", db_commentaire);
			}
			
			//definit les attributs
			request.setAttribute("msg", "Le commentaire a �t� supprim�. ");
			request.setAttribute("res_commentaire", db_commentaire.SuppressionCommentaire(id));

			RequestDispatcher rd = request.getRequestDispatcher("/admin_ok_delcomm.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			System.out.println("Erreur userServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}

