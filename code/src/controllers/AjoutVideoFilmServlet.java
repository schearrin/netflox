package controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import models.ActionVideoDB;

/**
 * Controleur pour l'ajout d'une video de catégorie Film
 * @author miary, sophie
 *
 */
@WebServlet("/AjoutVideoFilmServlet")
public class AjoutVideoFilmServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	//private final String UPLOAD_DIRECTORY = "C:/Users/Miary/Documents/projet-netflox/code/WebContent/video";
	private final String UPLOAD_DIRECTORY1 = "C:/Users/sophie/Bitbucket/projet-netflox/code/WebContent/video";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Lanson/Documents/netflox/code/WebContent/video";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Tongasoa/Bitbucket/projet-netflox/code/WebContent/video";

	//private final String UPLOAD_DIRECTORY = "C:/Users/Miary/Documents/projet-netflox/code/WebContent/images/videos/Film";
	private final String UPLOAD_DIRECTORY2 = "C:/Users/sophie/Bitbucket/projet-netflox/code/WebContent/images/videos/Film";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Lanson/Documents/netflox/code/WebContent/images/videos/Film";
	//private final String UPLOAD_DIRECTORY = "C:/Users/Tongasoa/Bitbucket/projet-netflox/code/WebContent/images/videos/Film";

	public AjoutVideoFilmServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Initialise variable pour recuperer les parametres du formulaire
		String param_titre = "";
		String param_auteur = "";
		String param_annee = "";
		String param_prix = "";
		String param_etat = "";
		String param_categorie = "";
		String param_genre = "";
		String param_motscles = "";
		String param_synopsis = "";

		int annee = 0;
		int prix = 0;

		//Initialise les variables pour le nom de l'image et le nom de la video
		String name_img="";
		String name_video="";

		//Si encryption = multipart content
		if(ServletFileUpload.isMultipartContent(request)){

			try{
				//Recupere connexion
				ActionVideoDB db_vaddfilm = (ActionVideoDB) request.getSession().getAttribute("db_vaddfilm");
				if (db_vaddfilm == null) {

					db_vaddfilm = new ActionVideoDB();
					request.getSession().setAttribute("db_vaddfilm", db_vaddfilm);

				}

				//Releve tout les items de la requete dans une liste
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);


				for(FileItem item : multiparts){
					//Si c'est le type est un fichier et non type dans formulaire
					if(!item.isFormField()){

						//Expression regulieres qui commence par caratere non blanc, finissent par ces extentions et insensible a la casse
						String regex_img = "([^\\s]+(\\.(?i)(bmp|jpg|gif|png))$)";
						String regex_video = "([^\\s]+(\\.(?i)(mp4|avi|wmv|mkv))$)";

						String name = new File(item.getName()).getName();
						System.out.println(name);

						//Si le pattern image est correct alors :
						if(Pattern.matches(regex_img, name)){
							item.write( new File(UPLOAD_DIRECTORY2 + File.separator + name));

							name_img = name;
							System.out.println("image upload ok");
						}else{System.out.println("image upload pas ok");}

						//Si le pattern video est correct alors :
						if(Pattern.matches(regex_video, name)){
							item.write( new File(UPLOAD_DIRECTORY1 + File.separator + name));

							name_video = name;
							System.out.println("video upload ok");
						}else{System.out.println("video upload pas ok");}
					}

					//Si le type est un type du formulaire mais non un fichier
					if(item.isFormField()){
						String fieldname = item.getFieldName();
						System.out.println(fieldname);

						if(fieldname.equals("titre")){
							param_titre = item.getString();
						}
						if(fieldname.equals("auteur")){
							param_auteur = item.getString();
						}
						if(fieldname.equals("annee")){
							param_annee = item.getString();
						}
						if(fieldname.equals("prix")){
							param_prix = item.getString();
						}
						if(fieldname.equals("etat")){
							param_etat = item.getString();
						}
						if(fieldname.equals("categorie")){
							param_categorie = item.getString();
						}
						if(fieldname.equals("genre")){
							param_genre = item.getString();
						}
						if(fieldname.equals("motscles")){
							param_motscles = item.getString();
						}
						if(fieldname.equals("synopsis")){
							param_synopsis = item.getString();
						}
					}

				}

				//Convertit string vers int pour annee et prix
				try {
					annee = Integer.parseInt(param_annee);
					prix = Integer.parseInt(param_prix);
				} catch (NumberFormatException e) {System.out.println("problem : number format exception");}

				//Definit les attributs
				request.setAttribute("res_addvideofilm",db_vaddfilm.addVideoDocumentaireFilm(param_titre, param_auteur, annee, prix, param_etat, param_categorie, param_genre, param_motscles, name_img, param_synopsis, name_video));

				//Redirection vers page jsp
				RequestDispatcher rd = request.getRequestDispatcher("/admin_addfilm.jsp");;
				rd.forward(request, response);

				System.out.println("Ok AjoutVideoFilmServlet");

			}catch(Exception e){
				System.out.println("Erreur AjoutVideoFilmServlet");
				e.printStackTrace();
				throw new ServletException(e);
			}
		}

	}

}
