package controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.ActionClientDB;
import models.StatistiquesDB;

/**
 * Controleur pour l'achat d'une video : ajoute une vid�o dans la base et incr�mente le nombre de t�l�chargement
 * @author sophie
 *
 */
@WebServlet("/AchatVideoServlet")
public class AchatVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AchatVideoServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();

    	String param_idv = (String) session.getAttribute("sessionIDV");
    	int idv = Integer.parseInt(param_idv);
    	System.out.println(idv);
    	String param_idu = (String) session.getAttribute("IDu");
    	int idu = Integer.parseInt(param_idu);
    	System.out.println(idu);

    	java.util.Date date= new java.util.Date();
    	Timestamp current_time = new Timestamp(date.getTime());

    	try{
    		//1) achat
    		ActionClientDB db_aclt2 = (ActionClientDB) request.getSession().getAttribute("db_aclt2");
    		if (db_aclt2 == null) {

    			db_aclt2 = new ActionClientDB();
    			request.getSession().setAttribute("db_aclt2", db_aclt2);

    		}

    		//Definit les attributs
    		request.setAttribute("res_achat", db_aclt2.paiementAchatLocation("Achat", idu, idv, current_time));
    		
			//2) statistiques
			StatistiquesDB db_stat = (StatistiquesDB) request.getSession().getAttribute("db_stat");
			if (db_stat == null) {

				db_stat = new StatistiquesDB();
				request.getSession().setAttribute("db_stat", db_stat);

			}
			
			//Incremente le nombre de telechargement
			db_stat.incrementNbTelechargement(idv);
			int nbt = db_stat.getNbTelechargementVideo(idv);
			System.out.println("Nombre de telechargement de cette video :"+nbt);
			
    		//Redirection vers la page jsp
    		RequestDispatcher rd = request.getRequestDispatcher("/client_respachat.jsp");;
    		rd.forward(request, response);

    		System.out.println("Ok AchatVideoServlet : Get");

    	} catch (Exception e) {

    		System.out.println("Erreur AchatVideoServlet : Get");
    		e.printStackTrace();
    		throw new ServletException(e);
    	}
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Recupere les parametres du formulaire
		String param_typeCB = request.getParameter("typeCB");
		String param_numCB = request.getParameter("numCB");
		String param_cryptoCB = request.getParameter("cryptoCB");
		String param_mois = request.getParameter("mois");
		String param_annee = request.getParameter("annee");

//		int numCB = 0;
		long numCB = 0;
		int cryptoCB = 0;
		int mois = 0;
		int annee = 0;
		try {
//			numCB = Integer.parseInt(param_numCB);
			numCB = Long.parseLong(param_numCB);
			cryptoCB = Integer.parseInt(param_cryptoCB);
			mois = Integer.parseInt(param_mois);
			annee = Integer.parseInt(param_annee);
		} catch (NumberFormatException e) {System.out.println("problem : number format exception");}

		try {
			//Recupere connexion
			ActionClientDB db_aclt2 = (ActionClientDB) request.getSession().getAttribute("db_aclt2");
			if (db_aclt2 == null) {

				db_aclt2 = new ActionClientDB();
				request.getSession().setAttribute("db_aclt2", db_aclt2);

			}

			request.setAttribute("verif", db_aclt2.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee));
			boolean verif = db_aclt2.verificationPaiement(param_typeCB, numCB, cryptoCB, mois, annee);

			if(verif){
				doGet(request, response);

			}else{
				//Redirection vers la page jsp avec message erreur
				request.setAttribute("erreur_paiement", "Erreur paiement, veuillez verifier vos informations.");

				RequestDispatcher rd = request.getRequestDispatcher("/client_respachat.jsp");;
				rd.forward(request, response);
			}

			System.out.println("Ok AchatVideoServlet : Post");

		} catch (Exception e) {
			System.out.println("Erreur AchatVideoServlet : Post");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

}
