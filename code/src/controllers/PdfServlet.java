package controllers;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import libs.genPdf;
import tools.ConnexionDB;
import beans.StatistiquesVideo;
import beans.Video;
import beans.Utilisateur;

/**
 * Classe PdfServlet
 * @author lanson
 * g�nere 2 PDF un Catalogue et un audit avec les statistiques des nombres de vues et de telechargements(controleur)
 */


@WebServlet("/PdfServlet")
public class PdfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public PdfServlet() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String param_file1 = request.getParameter("file1");
		//String param_file2 = request.getParameter("file2");
		
		try {
			//Recupere connexion
			genPdf db3 = (genPdf) request.getSession().getAttribute("db3");
			if (db3 == null) {

				db3 = new genPdf();
				request.getSession().setAttribute("db3", db3);

			}
			//definit les attributs
			
			
			
			Statement stmt = (new ConnexionDB()).conn.createStatement();
            ResultSet res = stmt.executeQuery("SELECT * FROM VIDEO");
            
            
          
            
            List<Video> lv = new LinkedList<Video> ();
            
            while (res.next()){
            	lv.add (new Video (res.getString("titre"), res.getString("auteur"), res.getInt("annee"), res.getInt("prix"),
        				res.getString("etat"), res.getString("categorie"),res.getString("genre"), res.getString("motscles"), res.getString("nom_img"),
        				res.getString("synopsis"), res.getString("nom_video")));
            	
            }
            
            
            Statement stmt1 = (new ConnexionDB()).conn.createStatement();
            ResultSet res1 = stmt1.executeQuery("SELECT * FROM UTILISATEUR");

            List<Utilisateur> lu = new LinkedList<Utilisateur> ();
            
            while (res1.next()){
            	lu.add (new Utilisateur (res1.getString("nom"), res1.getString("prenom"), res1.getString("adresse"), res1.getInt("telephone"), res1.getString("login") 
            			+ res1.getString("mdp"), res1.getString("type"), null));
            	
            }
            
            
			

			

		
		  Statement stmt2 = (new ConnexionDB()).conn.createStatement();
          ResultSet res2 = stmt2.executeQuery("SELECT * FROM STATISTIQUES_VIDEO");

          List<StatistiquesVideo> ls = new LinkedList<StatistiquesVideo> ();
          
          while (res2.next()){
          	ls.add (new StatistiquesVideo (res2.getInt("id_video"), res2.getInt("nb_vue"), res2.getInt("nb_telechargement")));
          	
          }
          
          
       
          
          libs.genPdf.generatePDFListV("C:\\PDF\\Catalogue.pdf", lv);
          libs.genPdf.generatePDFListU("C:\\PDF\\AuditetStat.pdf", lu, ls);
          

          RequestDispatcher rd = request.getRequestDispatcher("/PDF.jsp");
			rd.forward(request, response);
			
		} catch (Exception e) {
			System.out.println("Erreur pdfServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
		
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

}
