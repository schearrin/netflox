package controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Controleur pour uploader une image pour un Documentaire. Mais cette fonctionnalit� ne marche pas
 * @author miary
 *
 */

@SuppressWarnings("serial")
@WebServlet("/ImportImageDocumentaireServlet")
public class ImportImageDocumentaireServlet extends HttpServlet {
   private final String UPLOAD_DIRECTORY = "C:/Users/Miary/Documents/projet-netflox/code/WebContent/images/videos/Documentaire";
 
   @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
     
       //process only if its multipart content
       if(ServletFileUpload.isMultipartContent(request)){
           try {
               List<FileItem> multiparts = new ServletFileUpload(
                                        new DiskFileItemFactory()).parseRequest(request);
             
               for(FileItem item : multiparts){
                   if(!item.isFormField()){
                       String name = new File(item.getName()).getName();
                       item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
                   }
               }
          
              //File uploaded successfully
              request.setAttribute("message", "Image upload�e");
           } catch (Exception ex) {
              request.setAttribute("message", "Erreur � cause de " + ex);
           }          
        
       }else{
           request.setAttribute("message",
                                "D�sol�");
       }
   
       request.getRequestDispatcher("/admin_adddoc.jsp").forward(request, response);
    
   }
 
}