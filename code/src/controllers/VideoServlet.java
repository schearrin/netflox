package controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.ConsultationVideoDB;

/**
 * Controleur qui affiche la liste des videos selon la categorie et le genre
 * @author sophie
 *
 */
@WebServlet("/VideoServlet")
public class VideoServlet extends HttpServlet {

	private static final long serialVersionUID = 6133161013013907257L;


	public VideoServlet() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Recupere les parametres categorie et genre
		String param_categorie = request.getParameter("categorie");
		String param_genre = request.getParameter("genre");

		
		//Recupere les paramatres from et to
		String param_from = request.getParameter("from");
		String param_to = request.getParameter("to");

		int from = 0;
		try {
			from = Integer.parseInt(param_from);
		} catch (NumberFormatException e) {
		}
		
		int to = from + 10;

		try {
			to = Integer.parseInt(param_to);
		} catch (NumberFormatException e) {
		}


		try {
			//Recupere connexion
			ConsultationVideoDB dbvideo = (ConsultationVideoDB) request.getSession().getAttribute("dbvideo");
			if (dbvideo == null) {

				dbvideo = new ConsultationVideoDB();
				request.getSession().setAttribute("dbvideo", dbvideo);

			}


			//Definit les attributs
			request.setAttribute("param_categorie", new String(param_categorie));
			request.setAttribute("param_genre", new String(param_genre));
			request.setAttribute("to", new Integer(to));
			request.setAttribute("from", new Integer(from));
			
			request.setAttribute("nbVideo", dbvideo.getNombreVideos(param_categorie, param_genre));
			request.setAttribute("res_videos", dbvideo.getVideoList(param_categorie, param_genre, from, to));
			
			
			//Redirection vers la page jsp
			RequestDispatcher rd = request.getRequestDispatcher("/videos.jsp");;
			rd.forward(request, response);
			
			System.out.println("Ok VideoServlet");

		} catch (Exception e) {
			System.out.println("Erreur VideoServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

