package controllers;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.CommentaireDB;

/**
 * Controleur pour l'ajout d'un commentaire
 * @author sophie
 */
@WebServlet("/CommentaireServlet")
public class CommentaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CommentaireServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Recupere parametres
		HttpSession session = request.getSession();
		String param_prenom = (String) session.getAttribute("PRENOMu");
		String param_nom = (String) session.getAttribute("NOMu");

		java.util.Date date= new java.util.Date();
		Timestamp current_time = new Timestamp(date.getTime());

		String param_idv = (String) session.getAttribute("sessionIDV");
		int idv = 0;
		try {
			idv = Integer.parseInt(param_idv);
		} catch (NumberFormatException e) {
		}

		String param_idu = (String) session.getAttribute("IDu");
		int idu = 0;
		try {
			idu = Integer.parseInt(param_idu);
		} catch (NumberFormatException e) {
		}
		
		
		try{
			
			//1) Ajout et affichage des commentaires
			CommentaireDB db_vadcom = (CommentaireDB) request.getSession().getAttribute("db_vadcom");
				if (db_vadcom == null) {

					db_vadcom = new CommentaireDB();
					request.getSession().setAttribute("db_vadcom", db_vadcom);

				}
				
			
		String param_titre = request.getParameter("titre_commentaire");
		String param_texte = request.getParameter("texte_commentaire");
		
		//Definit les attributs
		request.setAttribute("add_commentaire", db_vadcom.AjoutCommentaire(idv, idu, param_titre, param_nom, param_prenom, param_texte, current_time));
	
		
		//Redirection vers la page jsp
		RequestDispatcher rd = request.getRequestDispatcher("/commentaire_ok.jsp");;
		rd.forward(request, response);
		
		
		System.out.println("Ok CommentaireServlet");
	
		}catch(Exception e){
			System.out.println("Erreur CommentaireServlet");
			e.printStackTrace();
			throw new ServletException(e);
		}
		doGet(request, response);
	}

}
