package tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnexionDB {

	private static String driver = "org.postgresql.Driver";
	private static String url = "jdbc:postgresql:postgres";
	private static String user = "postgres";
	private static String password = "1234";

	public static Connection conn;

	public ConnexionDB(){
		conn = null;

		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, password);

		}catch(Exception e){
			System.out.println("Probleme de connexion a la base de donnees : ");
			e.printStackTrace();
		}
	}

	/**
	 * Connexion a la base de donnees
	 */
	public Connection getConnexionDB(){
		return conn;
	}

	/**
	 * Cree un Statement pour les requetes select
	 */
	public Statement createStatement() throws SQLException {
		return conn.createStatement();
	}

	/**
	 * Cree un statement prepare pour les requetes insert
	 */
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return conn.prepareStatement(sql);
	}	
}
