package beans;

/**
 * Classe StatistiquesVideo
 * @author miary, sophie
 */

public class StatistiquesVideo {
	
	protected int id_video;

	protected int nb_vue;
	protected int nb_telechargement;
	
	public StatistiquesVideo (int id_video, int nb_vue,int nb_telechargement){
		this.id_video = id_video;
		this.nb_vue= nb_vue;
		this.nb_telechargement= nb_telechargement;
	}
	
	public int getId(){
		return id_video;
	}
	
	public int getNbVue(){
		return nb_vue;
	}
	
	public int getNbTelechargement() {
		return nb_telechargement;
	}
}