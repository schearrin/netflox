package beans;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe Video
 * @author lanson, sophie
 */

public class Video implements java.io.Serializable {

	private static final long serialVersionUID = -553782117110420447L;
	
	protected static AtomicInteger currentId = new AtomicInteger();
	protected int id_video;
	
	protected String titre;
	protected String auteur;
	protected int annee;
	protected int prix;
	protected String etat;
	
	protected String categorie;
	protected String genre;
	protected String motscles;
	
	protected String nom_img;
	protected String synopsis;
	
	protected String nom_video;

	
	public Video (String titre, String auteur, int annee, int prix, String etat, String categorie, String genre, String motscles, String nom_img, String synopsis, String nom_video){
		this.id_video = currentId.getAndIncrement();
		
		this.titre = titre;
		this.auteur = auteur;
		this.annee = annee;
		this.prix = prix;
		this.etat = etat;
		
		this.categorie = categorie;
		this.genre = genre;
		this.motscles = motscles;
		
		this.nom_img = nom_img;
		this.synopsis = synopsis;
		
		this.nom_video = nom_video;
	}
	public int getId_video() {
		return id_video;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getAuteur() {
		return auteur;
	}
	
	public int getAnnee() {
		return annee;
	}

	public int getPrix() {
		return prix;
	}

	public String getEtat() {
		return etat;
	}

	public String getCategorie() {
		return categorie;
	}
	
	public String getGenre(){
		return genre;
	}

	public String getMotsCles() {
		return motscles;
	}

	public String getNom_img() {
		return nom_img;
	}

	public String getSynopsis() {
		return synopsis;
	}
	
	public String getNom_video(){
		return nom_video;
	}
	
}