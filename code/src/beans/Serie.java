package beans;

/**
 * Classe Serie
 * @author lanson
 */

public class Serie extends Video{

	private static final long serialVersionUID = 1L;

	private int saison;
	private int episode;

	public Serie(String titre, String auteur, int annee, int prix, String etat,
			String categorie, String genre, String mots_cles, String nom_img, String synopsis, String nom_video, int saison, int episode) {

		super(titre, auteur, annee, prix, etat, categorie, genre, mots_cles, nom_img, synopsis, nom_video);

		this.saison = saison;
		this.episode = episode;
	}


	public int getSaison() {
		return saison;
	}

	public int getEpisode() {
		return episode;
	}
}