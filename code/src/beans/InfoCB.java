package beans;

import java.sql.Date;

/**
 * Classe InfoCB
 * @author lanson, sophie
 */

public class InfoCB {
	private int id_utilisateur;
	
//	private int num_CB;
	private long num_CB;
	private String nom_CB;
	private String type_CB;
	private Date date_exp;
	private int crypto;
	
//	public InfoCB(int id_utilisateur, int num_CB, String nom_CB, String type_CB, Date date_exp, int crypto){
	public InfoCB(int id_utilisateur, long num_CB, String nom_CB, String type_CB, Date date_exp, int crypto){
		this.id_utilisateur = id_utilisateur;
		this.num_CB = num_CB;
		this.nom_CB = nom_CB;
		this.type_CB = type_CB;
		this.date_exp = date_exp;
		this.crypto = crypto;
	}
	
	public int getIdUtilisateur(){
		return id_utilisateur;
	}
	
//	public int getNumCB() {
	public long getNumCB() {
		return num_CB;
	}
	
	public String getNomCB() {
		return nom_CB;
	}
	
	public String getTypeCB() {
		return type_CB;
	}
	
	public Date getDateExp() {
		return date_exp;
	}
	
	public int getCrypto() {
		return crypto;
	}
}
