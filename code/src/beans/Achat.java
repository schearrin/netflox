package beans;

import java.sql.Timestamp;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe Achat
 * @author sophie
 */

public class Achat {
	
	private static final AtomicInteger currentId_Achat = new AtomicInteger();
	private int id_achat;
	
	private int id_utilisateur;
	private int id_video;
	private Timestamp date;
	
	public Achat(int id_utilisateur, int id_video, Timestamp date){
		this.id_achat = currentId_Achat.getAndIncrement();
		this.id_utilisateur = id_utilisateur;
		this.id_video = id_video;
		this.date = date;
	}
	
	public int getIdAchat(){
		return id_achat;
	}
	
	public int getIdUtilisateur(){
		return id_utilisateur;
	}
	
	public int getIdVideo(){
		return id_video;
	}
	
	public Timestamp getDate(){
		return date;
	}
}
