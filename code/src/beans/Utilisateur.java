package beans;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe Utilisateur
 * @author lanson, Tongasoa
 */

public class Utilisateur {

	protected static final AtomicInteger currentId_usr = new AtomicInteger();

	private  int id_utilisateur;
	private  String nom;
	private  String prenom;
	private  String adresse;
	private  int telephone;
	private  String login;
	private  String mdp;
	private  String type;

	public Utilisateur(String nom, String prenom, String adresse, int telephone, String login, String mdp, String type){

		this.id_utilisateur = currentId_usr.getAndIncrement();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.telephone = telephone;
		this.login = login;
		this.mdp = mdp;
		this.type = type;
	}
	//un bean doit avoir au moins un constructeur par d�faut, public et sans param�tres. Java l'ajoutera de lui-m�me si aucun constructeur n'est explicit� ;

	public Utilisateur() {}
	
	public int getId() {
		return id_utilisateur;
	}
	

	public String getNom(){
		return nom;
		}
	
	public void setNom(String nom){
		this.nom = nom;
}

	public String getPrenom(){
		return prenom;

		}
	public void setPrenom(String prenom){
		this.prenom = prenom;
	}
	
	public String getAdresse(){
		return adresse;
		}
	public void setAdresse(String adresse){
		this.adresse = adresse;
	}

	public int getTel(){
		return telephone;
		}
	
	public void setTel(int telephone){
		this.telephone = telephone;
		}
	
	public String getLogin(){
		return login;
		}
	
	public void setLogin(String login){
		this.login = login;
		}
	
	public String getMdp(){
		return mdp;
		}
	
	public void setMdp(String mdp){
		this.mdp = mdp;
	}
	
	public String getType(){
		return type;
		}
	public void setType(String type){
		this.type = type;
	}
		
	@Override
	public String toString()
	{
		return currentId_usr + " (" + nom + ", " + prenom + ", " +  adresse + ", "
				+ telephone + ", " + login + ", " + mdp +", " + type+")";
	}
}





