package beans;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe InfoSerie
 * @author sophie
 */

public class InfoSerie {
	
	protected static final AtomicInteger currentId_serie = new AtomicInteger();
	private int id_serie;
	
	private int id_video;
	private int saison;
	private int episode;
	
	public InfoSerie(int id_video, int saison, int episode){
		this.id_serie = currentId_serie.getAndIncrement();
		this.id_video = id_video;
		this.saison = saison;
		this.episode = episode;
	}
	
	public int getIdSerie(){
		return id_serie;
	}
	
	public int getIdVideo(){
		return id_video;
	}
	
	public int getSaison(){
		return saison;
	}
	
	public int getEpisode(){
		return episode;
	}
}
