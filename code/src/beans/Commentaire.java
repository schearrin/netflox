package beans;

import java.sql.Timestamp;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classe commentaire
 * @author miary, sophie
 *
 */
public class Commentaire {

	private static final AtomicInteger currentId_Commentaire = new AtomicInteger();
	private int id_commentaire;
	
	private int id_video;
	private int id_utilisateur;
	
	private String titre_commentaire;
	private String nom_utilisateur;
	private String prenom_utilisateur;
	
	private String texte;
	private Timestamp date_commentaire;
	
	
	public Commentaire(int id_video, int id_utilisateur, String titre_commentaire, String nom_utilisateur, String prenom_utilisateur, String texte, Timestamp date_commentaire){
		this.id_commentaire = currentId_Commentaire.getAndIncrement();
		
		this.id_video = id_video;
		this.id_utilisateur = id_utilisateur;
		
		this.titre_commentaire = titre_commentaire;
		this.nom_utilisateur = nom_utilisateur;
		this.prenom_utilisateur = prenom_utilisateur;
		
		this.texte = texte;
		this.date_commentaire = date_commentaire;
	}
	
	
	public int getId_commentaire() {
		return id_commentaire;
	}


	public int getId_video() {
		return id_video;
	}


	public int getId_utilisateur() {
		return id_utilisateur;
	}


	public String getTitre_commentaire() {
		return titre_commentaire;
	}


	public String getNom_utilisateur() {
		return nom_utilisateur;
	}


	public String getPrenom_utilisateur() {
		return prenom_utilisateur;
	}


	public String getTexte() {
		return texte;
	}


	public Timestamp getDate_commentaire() {
		return date_commentaire;
	}
	
}
