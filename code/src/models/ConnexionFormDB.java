package models;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import libs.SHA1;
import tools.ConnexionDB;

/**
 * 
 * @author tong
 *
 */
public final class ConnexionFormDB {
	/**
	 * Recupere le mot de passe de l'utilisateur
	 * 
	 * @author Tongasoa
	 * @param login
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return String
	 */
	
	public String recupereMdpUtilisateur (String login) throws SQLException{
		
		ConnexionDB conn = new ConnexionDB();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT mdp FROM utilisateur where login='"+login+"'");
		res.next();
		String motdepasse = res.getString("mdp");
		return motdepasse;
	}
	
	/**
	 * Recherche si le login existe.
	 * 
	 * @author Tongasoa
	 * @param login
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return boolean
	 */
	public boolean rechLogin (String login) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT count(*) as NUM FROM utilisateur where login='"+login+"'");
		res.next();
		int count = res.getInt("NUM");
		if(count == 1){
			return true;
		}
		return false;
	}
	
	/**
	 * Recupere le mot de passe de l'utilisateur
	 * 
	 * @author Tongasoa
	 * @param login,mdp
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return boolean
	 */
	public boolean connexionUtilisateur(String login, String mdp) throws SQLException{
		String hmdp = SHA1.hash(mdp);
		if(rechLogin(login)){
			String motdepasse = recupereMdpUtilisateur(login);
			if (motdepasse.equals(hmdp)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Recupere l'id de l'utilisateur
	 * @author sophie
	 * @param login
	 * @return
	 * @throws SQLException
	 */
	public String getIdUtilisateur(String login) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT id_utilisateur FROM utilisateur where login='"+login+"'");
		res.next();
		String id = res.getString("id_utilisateur");
		return id;
	}
	
	/**
	 * Recupere le type de l'utilisateur
	 * @param login
	 * @return
	 * @throws SQLException
	 */
	public String getTypeUtilisateur(String login) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT type FROM utilisateur where login='"+login+"'");
		res.next();
		String t = res.getString("type");
		return t;
	}
	
	
	/**
	 * Recupere le prenom de l'utilisateur
	 * @author sophie
	 * @param login
	 * @return
	 * @throws SQLException
	 */
	public String getPrenomUtilisateur(String login) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT prenom FROM utilisateur where login='"+login+"'");
		res.next();
		String p = res.getString("prenom");
		return p;
	}
	
	
	/**
	 * Recupere le nom de l'utilisateur
	 * @author sophie
	 * @param login
	 * @return
	 * @throws SQLException
	 */
	public String getNomUtilisateur(String login) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT nom FROM utilisateur where login='"+login+"'");
		res.next();
		String n = res.getString("nom");
		return n;
	}
	

	public static void main(String[] args) throws SQLException{
		ConnexionFormDB c = new ConnexionFormDB();
		boolean b = c.connexionUtilisateur("premtoto", "premtoto");
		if(b){
			System.out.println("ok");
		}
	}
	
}