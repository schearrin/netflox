package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;

import tools.ConnexionDB;

/**
 * Classe SupCltDB
 * @author lanson
 * supprime un client 
 */
public class SupCltDB {
	private ServletContext ctx = null;

	public SupCltDB(ServletContext ctx) {
		this.ctx = ctx;

	}
	public boolean supUser(int id_utilisateur) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();


		ctx.log(">>>> suppression UTILISATEUR " + id_utilisateur);


		PreparedStatement s = conn.prepareStatement("DELETE FROM UTILISATEUR WHERE id_utilisateur = '"+id_utilisateur+"' ");


		s.executeUpdate();
		return true;

	}

	public boolean checkID(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		ResultSet res =s.executeQuery("SELECT COUNT (*) AS ID FROM utilisateur WHERE id_utilisateur ="+ id +"");
		res.next();
		int count = res.getInt("ID");
		if(count == 1 ){
			return true;
		}
		return false;
	}






}