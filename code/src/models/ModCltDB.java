package models;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;

import libs.SHA1;
import tools.ConnexionDB;

/**
 * Classe ModCltDB
 * @author lanson
 * modifie un client  
 */ 
public class ModCltDB {
	private ServletContext ctx = null;

	public ModCltDB(ServletContext ctx) {
		this.ctx = ctx;

	}
	public boolean modUser(int id_utilisateur, String nom, String prenom,String adresse, int telephone,String login, String mdp,  String type) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();


		ctx.log(">>>> Modification UTILISATEUR " + nom );

		String hmdp = SHA1.hash(mdp);
		
		PreparedStatement s = conn.prepareStatement("UPDATE UTILISATEUR SET nom = ? , prenom = ? , adresse = ? "
				+ " , telephone = ? , login = ? , mdp = ? "
				+ " , type = ? WHERE id_utilisateur = ? ");
		
		s.setString(1,nom);
		s.setString(2,prenom);
		s.setString(3,adresse);
		s.setInt(4,telephone);
		s.setString(5,login);
		s.setString(6,hmdp);
		s.setString(7,type);
		s.setInt(8,id_utilisateur);
		

		s.executeUpdate();
		return true;

	}
	public boolean checkID(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		ResultSet res =s.executeQuery("SELECT COUNT (*) AS ID FROM utilisateur WHERE id_utilisateur ="+ id +"");
		res.next();
		int count = res.getInt("ID");
		if(count == 1 ){
			return true;
		}
		return false;
	}
	
	
}
