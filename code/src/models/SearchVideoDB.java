package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map.Entry;

import beans.Video;
import tools.ConnexionDB;

public class SearchVideoDB {
	
	private ResultToGetDB res_toget = new ResultToGetDB();

	/**
	 * Retourne les resultats d'une recherche video sur les criteres : titre, categorie, genre et motscles
	 * 
	 * @author sophie
	 * @param recherche
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return Les resultats d'une recherche video
	 */
	public HashMap<Integer, Video> searchVideo(String txt) throws SQLException{	

		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Video> v = new HashMap<Integer,Video>();

		Statement s = conn.createStatement();

		String requete = "select * from video where titre LIKE '%"+txt
				+"%' OR categorie LIKE '%"+txt+"%' OR genre LIKE '%"+txt
						+"%' OR motscles LIKE '%"+txt+"%' order by titre";

		ResultSet res = s.executeQuery(requete);

		while(res.next()){
			v.put(res.getInt("id_video"), res_toget.resulttogetVideoList(res));
		}

		return v;
	}
	
	public static void main(String[] args) throws SQLException{
		SearchVideoDB s = new SearchVideoDB();
		HashMap<Integer, Video> test = s.searchVideo("Star");
		
		for(Entry<Integer, Video> entry : test.entrySet()) {
		    Integer key = entry.getKey();
		    Video value = entry.getValue();
		    System.out.println(key+" : "+value);
		} 
	}
}
