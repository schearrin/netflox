package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;

import beans.Commentaire;
import tools.ConnexionDB;

public class CommentaireDB {
	
	private ResultToGetDB res_toget = new ResultToGetDB();
	
	/**
	 * Ajout de commentaire
	 * @author miary, sophie
	 * @param id_v
	 * @param id_u
	 * @param titre
	 * @param nom
	 * @param prenom
	 * @param texte
	 * @param date
	 * @return id du commentaire
	 * @throws SQLException
	 */
	public int AjoutCommentaire(int id_v, int id_u, String titre, String nom, String prenom, String texte, Timestamp date) throws SQLException{
		
		ConnexionDB conn = new ConnexionDB();
		
		String requete = "INSERT INTO commentaire(id_video, id_utilisateur, titre_commentaire, nom_utilisateur, prenom_utilisateur, texte, date_commentaire) VALUES (?,?,?,?,?,?,?) RETURNING id_commentaire";
		PreparedStatement s = conn.prepareStatement(requete);
		
		s.setInt(1, id_v);
		s.setInt(2, id_u);
		s.setString(3, titre);
		s.setString(4, nom);
		s.setString(5, prenom);
		s.setString(6, texte);
		s.setTimestamp(7, date);
		
		ResultSet res = s.executeQuery();
		
		res.next();
		int currentId = res.getInt("id_commentaire");
		
		return currentId;
	} 
	
	/**
	 * Suppression de commentaire selon l'id du commentaire
	 * @author miary
	 * @param id_commentaire
	 * @return
	 * @throws SQLException
	 */
	public boolean SuppressionCommentaire(int id_commentaire) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		String requete = "DELETE FROM commentaire WHERE id_commentaire="+id_commentaire;
		PreparedStatement s = conn.prepareStatement(requete);
		s.executeUpdate();

		s.close();

		return true;
	}
	
	/**
	 * Affiche les commentaires selon l'id d'une video
	 * @author sophie
	 * @param id_video
	 * @return
	 * @throws SQLException
	 */
	public HashMap<Integer, Commentaire> AfficheCommentaire(int id_video) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Commentaire> v = new HashMap<Integer,Commentaire>();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM commentaire where id_video = " + id_video);
		while (res.next()){
			v.put(res.getInt("id_commentaire"), res_toget.resulttogetCommentaire(res));
		}

		return v;		
	}
	
	

}