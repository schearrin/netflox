package models;

import tools.ConnexionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import beans.Video;
import models.ResultToGetDB;

public class ConsultationVideoDB {

	private int nbVideo;
	private ResultToGetDB res_toget = new ResultToGetDB();
	
	/**
	 * Retourne la liste des videos selon la categorie et le genre
	 * 
	 * @author sophie
	 * @param categorie
	 * @param genre
	 * @param from
	 * @param to
	 * @return La liste des videos selon la categorie et le genre
	 * @throws SQLException
	 */
	public HashMap<Integer,Video> getVideoList(String categorie, String genre, int from, int to) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Video> v = new HashMap<Integer,Video>();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM video WHERE categorie='"
				+categorie+"' AND genre='"+genre+"' ORDER BY titre LIMIT "
				+(to-from)+ " OFFSET "+from);

		while (res.next()){
			v.put(res.getInt("id_video"), res_toget.resulttogetVideoList(res));
		}

		return v;
	}


	/**
	 * Retourne le nombre total de video selon la categorie et le genre
	 * 
	 * @author sophie
	 * @param categorie
	 * @param genre
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return Nombre total de video selon la categorie et le genre
	 */
	public int getNombreVideos(String categorie, String genre) throws SQLException{
		nbVideo = -1;

		if (nbVideo == -1){
			ConnexionDB conn = new ConnexionDB();
			Statement s = conn.createStatement();
			ResultSet res = s.executeQuery("select count(*) as NUM from video where categorie='"
					+categorie+"' and genre='"+genre+"'");

			res.next();
			nbVideo = res.getInt("NUM");
		}

		return nbVideo;
	}


	/**
	 * Retourne le detail d'une video
	 * 
	 * @author sophie
	 * @param id
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return Le detail d'une vid�o
	 */
	public HashMap<Integer, Video> getVideoDetail(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Video> v = new HashMap<Integer,Video>();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM VIDEO WHERE id_video="+id);

		res.next();
		v.put(res.getInt("id_video"), res_toget.resulttogetVideoList(res));

		return v;
	}	
}
