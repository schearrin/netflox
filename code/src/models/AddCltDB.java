package models;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContext;

import libs.SHA1;
import tools.ConnexionDB;
/**
 * Classe AddCltDB
 * @author lanson
 * ajoute un client 
 */
public class AddCltDB {
	private ServletContext ctx = null;

	public AddCltDB(ServletContext ctx) {
		this.ctx = ctx;

	}
	public boolean addUser(String nom, String prenom,String adresse, int telephone, String login, String mdp, String type) throws SQLException
	{
		//Connexion � la base de donn�es
		ConnexionDB conn = new ConnexionDB();


		ctx.log(">>>> INSERTION UTILISATEUR " + nom);
		
		String hmdp = SHA1.hash(mdp);
		 
		//L'instantation de la requ�te INSERT
		PreparedStatement s = conn.prepareStatement("INSERT INTO UTILISATEUR( nom, prenom, adresse,telephone, login, mdp, type) "
				+ "VALUES(?,?,?,?,?,?,?)");
		s.setString(1, nom);
		s.setString(2, prenom);
		s.setString(3, adresse);
		s.setInt(4, telephone);
		s.setString(5, login);
		s.setString(6, hmdp);
		s.setString(7, type);

		s.executeUpdate();
		return true;

	}



	/* public void  modifier(String id_usr,String nom,String prenom,String adresse,int telephone,String login, String mdp) throws SQLException{

		 ConnexionDB conn = new ConnexionDB();
			Statement s = conn.createStatement();
	       s.executeUpdate("UPDATE UTILISATEUR SET NOM='"+nom+"',PRENOM='"+prenom+"',ADRESSE='"+adresse+"',TELEPHONE='"+telephone+
	    		   										"',LOGIN='"+login+"',MDP='"+mdp+"' WHERE id_utlisateur='"+id_usr+"' ");



	    }

		public static void main(String[] args) throws SQLException{
			AddCltDB v = new AddCltDB(ctx);

			v.addUser("test","test","z",123456,"z","t123","Premium");

		}*/


}

