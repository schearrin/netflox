package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Commentaire;
import beans.Video;

public class ResultToGetDB {
	
	/**
	 * @param res
	 * @return
	 * @throws SQLException
	 */
	public Video resulttogetVideoList(ResultSet res) throws SQLException{
		return new Video(res.getString("titre"), res.getString("auteur"), res.getInt("annee"), res.getInt("prix"),
				res.getString("etat"), res.getString("categorie"),res.getString("genre"), res.getString("motscles"), res.getString("nom_img"),
				res.getString("synopsis"), res.getString("nom_video"));
	}
	
	/**
	 * 
	 * @param res
	 * @return
	 * @throws SQLException
	 */
	public Commentaire resulttogetCommentaire(ResultSet res) throws SQLException{
		return new Commentaire(res.getInt("id_video"), 
				res.getInt("id_utilisateur"), 
				res.getString("titre_commentaire"),
				res.getString("nom_utilisateur"), 
				res.getString("prenom_utilisateur"), 
				res.getString("texte"), 
				res.getTimestamp("date_commentaire"));
	}
}
