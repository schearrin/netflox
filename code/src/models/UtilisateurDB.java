package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import beans.Utilisateur;

import tools.ConnexionDB;

/**
 * 
 * @author tong
 *
 */
public class UtilisateurDB {
	/**
	 * Chercher un utilisateur dans la base. 
	 * Cette classe a �t� cr��e dans le bus d'enregistrer un objet Utilisateur dans la session mais a �t� laiss� tomber car trop complexe.
	 * 
	 * @author Tongasoa
	 */
	public static HashMap<Integer, Utilisateur> searchUtilisateurByAdress(String txt) throws SQLException{	
		

		String[] tabmot = txt.split(" ");

		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Utilisateur> v = new HashMap<Integer,Utilisateur>();


		Statement s = conn.createStatement();

		for(String mot: tabmot){
			String requete = "select * from utilisateur where adresse ="+mot;
			ResultSet res = s.executeQuery(requete);
			
			while(res.next()){
				if(!v.containsKey(res.getInt("id_utilisateur"))){
					v.put(res.getInt("id_utilisateur"), resulttogetUtilisateurList(res));
				}
			}
		}
		return v;
	}
	
	public HashMap<Integer, Utilisateur> searchUtilisateurByMdp(String txt) throws SQLException{	
		

		String[] tabmot = txt.split(" ");

		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Utilisateur> v = new HashMap<Integer,Utilisateur>();


		Statement s = conn.createStatement();

		for(String mot: tabmot){
			String requete = "select * from utilisateur where mdp LIKE '%"+mot;
			ResultSet res = s.executeQuery(requete);
			
			while(res.next()){
				if(!v.containsKey(res.getInt("id_utilisateur"))){
					v.put(res.getInt("id_utilisateur"), resulttogetUtilisateurList(res));
				}
			}
		}
		return v;
	}
	
	
	
	public static Utilisateur resulttogetUtilisateurList(ResultSet res) throws SQLException{
		return new Utilisateur(res.getString("nom"), res.getString("prenom"), res.getString("adresse"), res.getInt("telephone"),
				res.getString("login"), res.getString("mdp"),res.getString("type"));
	}
	
	public static boolean validationAdresseDB (String adresse) throws SQLException{
		HashMap<Integer,Utilisateur> tab = searchUtilisateurByAdress(adresse);
		if(tab.isEmpty())
			return false;
		else return true;
	}

	public boolean validationMdpeDB (String mdp) throws SQLException{
		HashMap<Integer,Utilisateur> tab = searchUtilisateurByMdp(mdp);
		if(tab.isEmpty())
			return false;
		else return true;
	}
	
}
