package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import beans.Location;
import tools.ConnexionDB;

public class ActionClientDB {
	/**
	 * Ajoute une video dans la base
	 * 
	 * @author sophie
	 * @param titre, annee, duree, prix, etat, categorie, genre, motscles, nom_img, synopsis
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return true
	 */
	public boolean siLocationExisteDeja(int id_u, int id_v) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT count(*) as NUM FROM location WHERE id_video="+id_v+" and id_utilisateur="+id_u);

		res.next();
		int nbligne = res.getInt("NUM");
		if(nbligne >  0){
				return true;
		}
		return false;
	}
	
	
		/**
		 * Ajoute une location de video dans la base
		 * @author sophie
		 * @param idu
		 * @param idv
		 * @param date
		 * @return true
		 * @throws SQLException
		 */
	public boolean addLocation(int idu, int idv, Timestamp date) throws SQLException{
		
		ConnexionDB conn = new ConnexionDB();
		String requete = "INSERT INTO location (id_utilisateur, id_video, date_loc) VALUES (?,?,?)";
		
		PreparedStatement s = conn.prepareStatement(requete);
		
		s.setInt(1, idu);
		s.setInt(2, idv);
		s.setTimestamp(3, date);
		
		s.executeUpdate();
		s.close();
		return true;
	}
	
	/**
	 * Ajoute un achat de video dans la base
	 * @author sophie
	 * @param idu
	 * @param idv
	 * @param date
	 * @return true
	 * @throws SQLException
	 */
	public boolean addAchat(int idu, int idv, Timestamp date) throws SQLException{
		
		ConnexionDB conn = new ConnexionDB();
		String requete = "INSERT INTO achat (id_utilisateur, id_video, date_achat) VALUES (?,?,?)";
		
		PreparedStatement s = conn.prepareStatement(requete);
		
		s.setInt(1, idu);
		s.setInt(2, idv);
		s.setTimestamp(3, date);
		
		s.executeUpdate();
		s.close();
		
		return true;
	}
	
	/**
	 * Retourne le type d'un utilisateur
	 * @author sophie
	 * @param id
	 * @return le type d'un utilisateur
	 * @throws SQLException
	 */
	public String getTypeUtilisateur(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT type FROM utilisateur WHERE id_utilisateur="+id);
		res.next();
		String t= res.getString("type");
		return t;
	}
	
	/**
	 * Retourne les informations de location d'un utilisateur
	 * @author sophie
	 * @param iduti
	 * @param idvid
	 * @return
	 * @throws SQLException
	 */
	public HashMap<Integer, Location> getInformationLocation(int iduti, int idvid) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer,Location> v = new HashMap<Integer,Location>();
		
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM location WHERE id_utilisateur ="+iduti+" and id_video="+idvid);
		
		res.next();
		v.put(res.getInt("id_utilisateur"), new Location(res.getInt("id_utilisateur"), res.getInt("id_video"), res.getTimestamp("date")));
		return v;
	}
	
	/**
	 * Retourne true si les champs du verification paiement ne sont pas vide
	 * @author sophie
	 * @param typeCB
	 * @param numCB
	 * @param cryptoCB
	 * @param mois
	 * @param annee
	 * @return
	 * @throws SQLException
	 */
//	public boolean verificationPaiement(String typeCB, int numCB, int cryptoCB, int mois, int annee) throws SQLException{
	public boolean verificationPaiement(String typeCB, long numCB, int cryptoCB, int mois, int annee) throws SQLException{
		
		if(!(numCB == 0) || !(cryptoCB ==0) || !(mois==0) || !(annee==0)){
			return true;
		}
		return false;
	}
	
	/**
	 * Effectue l'ajout d'un achat ou location
	 * @author sophie
	 * @param action
	 * @param idu
	 * @param idv
	 * @param current_time
	 * @return
	 * @throws SQLException
	 */
	public boolean paiementAchatLocation(String action, int idu, int idv, Timestamp current_time) throws SQLException{
		
		ActionClientDB db_acltal = new ActionClientDB();
		boolean b = false;
		
		if(action.equals("Achat")){
			b = db_acltal.addAchat(idu, idv, current_time);
			return b;
		}
		if(action.equals("Location")){
			b= db_acltal.addLocation(idu, idv, current_time);
		}
		return b;
	}
	
	/**
	 * Met a jour la table de location en supprimant toutes les locations dont le temps depasse 10h
	 * @author sophie
	 * @throws SQLException 
	 */
	public void MAJ_deleteLocation() throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		
		//On recupere date courante et la date de toutes les locations
		ArrayList<Timestamp> tabnow = new ArrayList<Timestamp>();
		
		String selectnow = "SELECT NOW() as DTE";
		ResultSet res1 = s.executeQuery(selectnow);
		res1.next();
		tabnow.add(res1.getTimestamp("DTE"));
		
		
		HashMap<Integer, Timestamp> tabloc = new HashMap<Integer, Timestamp>();
		
		String selectloc = "SELECT id_loc, date_loc FROM location";
		ResultSet res2 = s.executeQuery(selectloc);
		
		while (res2.next()){
			tabloc.put(res2.getInt("id_loc"),res2.getTimestamp("date_loc"));
		}
		
		
		//Pour chaque date de location, on fait la difference avec la courante en heure
		 Timestamp date = tabnow.get(0);
		 
		for(Entry<Integer, Timestamp> entree : tabloc.entrySet()) {
		    Integer cle = entree.getKey();
		    Timestamp valeur = entree.getValue();
		    
		   
		    String select_diffdate=  "SELECT DATE_PART('day', '"+date+"'::timestamp - '"+valeur+"'::timestamp) * 24 "
		    		+ "+ DATE_PART('hour', '"+date+"'::timestamp - '"+valeur+"'::timestamp) as DIF";
		    
		    ResultSet res_diff = s.executeQuery(select_diffdate);
		    
		    res_diff.next();
		    int val_diff = res_diff.getInt("DIF");
		    
		    //Si cela depasse 10h alors on supprime
		    if(val_diff > 10){
		    	String delloc = "DELETE FROM location WHERE id_loc ="+cle;
		    	PreparedStatement p = conn.prepareStatement(delloc);
		    	p.executeUpdate();
		    }
		} 
	}
	
	
	public static void main(String args[]) throws SQLException{
		ActionClientDB test = new ActionClientDB();
		
		test.MAJ_deleteLocation();
	}
}
