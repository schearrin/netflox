package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tools.ConnexionDB;

public class ActionVideoDB {
	
	/**
	 * Verifie que la video existe dans la base
	 * 
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public boolean existeVideo(int id) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();
		
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT count(*) as NUM FROM video WHERE id_video="+id);

		res.next();
		int nbligne = res.getInt("NUM");
		if(nbligne >  0){
				return true;
		}
		return false;
	}
	
	/**
	 * Ajoute un documentaire dans la base
	 * 
	 * @author sophie
	 * @param titre, annee, duree, prix, etat, categorie, genre, motscles, nom_img, synopsis
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return true
	 */
	public boolean addVideoDocumentaireFilm(String titre, String auteur, int annee, int prix, String etat, String categorie, String genre, String motscles, String nom_img, String synopsis, String nom_video) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();

		String requete = "INSERT INTO video(titre,auteur,annee,prix,etat,categorie,genre,motscles,nom_img,synopsis,nom_video) VALUES(?,?,?,?,?,?,?,?,?,?,?) RETURNING id_video";
		//ajoute video
		PreparedStatement s = conn.prepareStatement(requete);

		s.setString(1, titre);
		s.setString(2, auteur);
		s.setInt(3, annee);
		s.setInt(4, prix);
		s.setString(5, etat);
		s.setString(6, categorie);
		s.setString(7, genre);
		s.setString(8, motscles);
		s.setString(9, nom_img);
		s.setString(10, synopsis);
		s.setString(11, nom_video);
		
		ResultSet res = s.executeQuery();
		
		res.next();
		int currentId = res.getInt("id_video");
	
				
		//ajoute statistique video
		s = conn.prepareStatement("INSERT INTO statistiques_video(id_video,nb_vue,nb_telechargement) VALUES (?,?,?)");
		
		s.setInt(1, currentId);
		s.setInt(2, 0);
		s.setInt(3, 0);
		
		s.executeUpdate();

		s.close();
		return true;
	}
	/**
	 * Ajoute une s�rie dans la base
	 * 
	 * @author sophie
	 * @param titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video, saison, �pisode
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return true
	 */
	public boolean addVideoSerie(String titre, String auteur, int annee, int prix, String etat, String categorie, String genre, String motscles, String nom_img, String synopsis, String nom_video, int saison, int episode) throws SQLException{
		
		ConnexionDB conn = new ConnexionDB();
		
		//ajout dans table video
		String requete1 = "INSERT INTO video(titre,auteur,annee,prix,etat,categorie,genre,motscles,nom_img,synopsis,nom_video) VALUES(?,?,?,?,?,?,?,?,?,?,?) RETURNING id_video";
		
		PreparedStatement s = conn.prepareStatement(requete1);
		
		s.setString(1, titre);
		s.setString(2, auteur);
		s.setInt(3, annee);
		s.setInt(4, prix);
		s.setString(5, etat);
		s.setString(6, categorie);
		s.setString(7, genre);
		s.setString(8, motscles);
		s.setString(9, nom_img);
		s.setString(10, synopsis);
		s.setString(11, nom_video);
		
		ResultSet res = s.executeQuery();
		
		res.next();
		int currentId = res.getInt("id_video");
		
		//ajout dans la table info_serie
		String requete2 = "INSERT INTO info_serie(id_video,saison,episode) VALUES (?,?,?)";
		
		s = conn.prepareStatement(requete2);
		
		s.setInt(1, currentId);
		s.setInt(2, saison);
		s.setInt(3, episode);
		
		s.executeUpdate();
		
		//ajout statistique video
		String requete3 = "INSERT INTO statistiques_video(id_video,nb_vue,nb_telechargement) VALUES (?,?,?)";
		s = conn.prepareStatement(requete3);
		
		s.setInt(1, currentId);
		s.setInt(2, 0);
		s.setInt(3, 0);
		
		s.executeUpdate();

		s.close();
		
		return true;
	}
	
	/**
	 * Suppression d'une video
	 *  
	 * @author sophie, miary
	 * @param id
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return true
	 */
	public boolean deleteVideo(int id) throws SQLException
	{	
		ActionVideoDB dvdb = new ActionVideoDB();

		if(dvdb.existeVideo(id)){
			
			ConnexionDB conn = new ConnexionDB();

			//supprime de la table statistique
			String requete1 = "DELETE FROM statistiques_video WHERE id_video="+id;
			PreparedStatement s = conn.prepareStatement(requete1);
			s.executeUpdate();

			//supprime de la table info_serie si c'est une serie

			String requete21 = "SELECT count(*) AS NUM FROM info_serie where id_video="+id;
			s = conn.prepareStatement(requete21);

			ResultSet res = s.executeQuery();
			res.next();
			int nbligne = res.getInt("NUM");

			if(nbligne >  0){
				String requete22 = "DELETE FROM info_serie WHERE id_video="+id;
				s = conn.prepareStatement(requete22);
				s.executeUpdate();
			}


			//supprime de la table video
			String requete3 = "DELETE FROM video WHERE id_video="+id;
			s = conn.prepareStatement(requete3);
			s.executeUpdate();

			s.close();
			
			return true;
			
		}else{

			return false;
		}
	}
}
