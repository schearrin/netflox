package models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import beans.StatistiquesVideo;
import tools.ConnexionDB;

public class StatistiquesDB {
	
	/**
	 * Retourne pour chaque videos le nombre de fois ou elle a ete consultee et telechargee
	 * 
	 * @author sophie
	 * @param id
	 * @throws SQLException Si une erreur survient lors de l'execution de la requete
	 * @return nombre de consultation et telechargement
	 */
	public HashMap<Integer, StatistiquesVideo> getStatistiquesVideo(int id) throws SQLException
	{
		ConnexionDB conn = new ConnexionDB();
		HashMap<Integer, StatistiquesVideo> v = new HashMap<Integer, StatistiquesVideo>();

		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM statistiques_video WHERE id_video="
				+id);
		
		while (res.next()){
			v.put(res.getInt("id_video"), new StatistiquesVideo(res.getInt("id_video"), res.getInt("nb_vue"), res.getInt("nb_telechargement")));
		}
		
		return v;
		
	}
	
	/**
	 * Retourne le nombre de vue pour une video
	 * 
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public int getNbVueVideo(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		
		ResultSet res = s.executeQuery("SELECT nb_vue FROM statistiques_video WHERE id_video="+id);
		
		res.next();
		return res.getInt("nb_vue");
	}
	
	/**
	 * Incremente le nombre de vue
	 * 
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public boolean incrementNbVue(int id) throws SQLException
	{
		int incNbVue = getNbVueVideo(id) + 1;
		ConnexionDB conn = new ConnexionDB();
		PreparedStatement s = conn.prepareStatement("UPDATE statistiques_video SET nb_vue="+incNbVue+" WHERE id_video="+id);
		
		s.executeUpdate();
		return true;
	}
	
	
	/**
	 * Retourne le nombre de telechargement pour une video
	 * 
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public int getNbTelechargementVideo(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		Statement s = conn.createStatement();
		
		ResultSet res = s.executeQuery("SELECT nb_telechargement FROM statistiques_video WHERE id_video="+id);
		
		res.next();
		return res.getInt("nb_telechargement");
	}
	
	/**
	 * Incremente le nombre de telechargement
	 * 
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public boolean incrementNbTelechargement(int id) throws SQLException
	{
		int incNbT = getNbTelechargementVideo(id) + 1;
		ConnexionDB conn = new ConnexionDB();
		PreparedStatement s = conn.prepareStatement("UPDATE statistiques_video SET nb_telechargement="+incNbT+" WHERE id_video="+id);
		
		s.executeUpdate();
		return true;
	}
	
}
