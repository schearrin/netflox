package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import beans.Video;
import tools.ConnexionDB;

public class SuggestionVideoDB {
	
	private ResultToGetDB res_toget = new ResultToGetDB();
	
	/**
	 * Retourne la categorie d'une vid�o
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public String getCategorieVideo(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT categorie FROM video WHERE id_video="+id);
		res.next();
		String cat = res.getString("categorie");
		return cat;	
	}
	
	/**
	 * Retourne le genre d'une vid�o
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public String getGenreVideo(int id) throws SQLException{
		ConnexionDB conn = new ConnexionDB();
		
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT genre FROM video WHERE id_video="+id);
		res.next();
		String gen = res.getString("genre");
		return gen;	
	}

	/**
	 * Selectionne tout les id videos prive de l'id que l'on a selectionne
	 * @author sophie
	 * @param id
	 * @param categorie
	 * @param genre
	 * @return
	 * @throws SQLException
	 */
	public HashMap<Integer, Video> selectallId_privedeId(int id, String categorie, String genre) throws SQLException{
		ConnexionDB conn = new ConnexionDB();

		HashMap<Integer, Video> tab = new HashMap<Integer, Video>();
		Statement s = conn.createStatement();
		ResultSet res = s.executeQuery("SELECT * FROM video WHERE categorie='"+
				categorie+"' and genre='"+genre+"' and id_video !="+id+" LIMIT 7 OFFSET 0");

		while(res.next()){
			tab.put(res.getInt("id_video"), res_toget.resulttogetVideoList(res));
		}

		return tab;
	}
	
	/**
	 * Suggere des video selon la categorie et le genre
	 * @author sophie
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public HashMap<Integer, Video> suggestionVideo(int id) throws SQLException{
		
		SuggestionVideoDB db_s = new SuggestionVideoDB();
		
		String categorie = db_s.getCategorieVideo(id);
		String genre = db_s.getGenreVideo(id);
		
		HashMap<Integer, Video> hm = db_s.selectallId_privedeId(id, categorie, genre);
		
		return hm;
		
	}

}
