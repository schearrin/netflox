package libs;

import java.io.*;

import javax.servlet.http.HttpServlet;

import beans.StatistiquesVideo;
import beans.Utilisateur;
import beans.Video;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class genPdf extends HttpServlet {  

	/**
	 * Classe genPdf
	 * @author lanson
	 * g�nere 2 PDF un Catalogue et un audit avec les statistiques des nombres de vues et de telechargements
	 */
	private static final long serialVersionUID = 1L;

	//public static final String DEST1 = "resources/ok.pdf";
	//public static final String DEST2 = "resources/ok1.pdf";



	/*public boolean createListVideo(String filename)throws DocumentException, IOException, SQLException {

                ConnexionDB conn = new ConnexionDB();


                Statement stmt = conn.createStatement();

                ResultSet res = stmt.executeQuery("SELECT id_video, titre, annee FROM VIDEO");

                Document document = new Document();

                PdfWriter.getInstance(document, new FileOutputStream("F:\\PDF\\ok.pdf"));
                document.open();            

                //System.out.println(System.getProperty("java.io.tmpdir"));
                PdfPTable table = new PdfPTable(3);

                PdfPCell table_cell;

                while (res.next()) {                
                                String id_video = res.getString("id_video");
                                table_cell=new PdfPCell(new Phrase(id_video));
                                table.addCell(table_cell);
                                String titre=res.getString("titre");
                                table_cell=new PdfPCell(new Phrase(titre));
                                table.addCell(table_cell);
                                String annee=res.getString("annee");
                                table_cell=new PdfPCell(new Phrase(annee));
                                table.addCell(table_cell);
                                //document.newPage();
                                String duree=res.getString("duree");
                                table_cell=new PdfPCell(new Phrase(duree));
                                table.addCell(table_cell);
                                }

                document.add(table);                       
                document.close();


                res.close();
                stmt.close();
				return true; 
		}  */  


	public static void generatePDFListV(String path, java.util.List<Video> lv) {

		Document document = new Document();
		File MyFile = new File(path);
		MyFile.delete();

		try {
			PdfWriter.getInstance(document, new FileOutputStream(path, false));



			document.open();
			//Cr�e un titre Catalogue

			Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
			Chunk chunk = new Chunk("Catalogue", chapterFont);
			Chapter chapter = new Chapter(new Paragraph(chunk), 1);
			chapter.setNumberDepth(0);

			//Cr�e une liste 

			List orderedList = new List(List.ORDERED);
			for (Video v : lv) {
				orderedList.add(new ListItem(": "
						+ v.getTitre() + " , " +v.getAuteur() + " , " +
						+ v.getAnnee() + " , " +v.getPrix()+ " , "  
						+ v.getEtat()+ " , " + v.getCategorie()+ " , "
						+ v.getGenre()+ " , "+ v.getMotsCles()+" , "
						+ v.getSynopsis()+". "));
			}
			document.add(chapter);
			document.add(orderedList);
			document.close();
		} catch (DocumentException e) {
		} catch (FileNotFoundException e) {
		}

	}

	public static void generatePDFListU(String path, java.util.List<Utilisateur> lu, java.util.List<StatistiquesVideo> ls) {

		Document document = new Document();
		File MyFile = new File(path);
		MyFile.delete();

		try {
			PdfWriter.getInstance(document, new FileOutputStream(path, false));



			document.open();
			//Cr�e un titre Audit

			Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
			Chunk chunk = new Chunk("Audit", chapterFont);
			Chapter chapter = new Chapter(new Paragraph(chunk), 1);
			chapter.setNumberDepth(0);

			//Cr�e une liste 

			List Listu = new List(List.ORDERED);
			for (Utilisateur u : lu) {
				Listu.add(new ListItem(": "
						+ u.getNom() + " , " 
						+ u.getPrenom() + " , " 
						+ u.getTel()+ " , "
						+ u.getLogin()+ " , "  
						+ u.getMdp()+ " , "  
						+ u.getType()+ ". " ));
			}

			Font chapterFont1 = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
			Chunk chunk1 = new Chunk("Statistique", chapterFont1);
			Chapter chapter1 = new Chapter(new Paragraph(chunk1), 1);
			chapter.setNumberDepth(0);

			List Lists = new List(List.ORDERED);
			for (StatistiquesVideo s : ls) {
				Lists.add(new ListItem("| "
						+ s.getNbVue() + " | " 
						+ s.getNbTelechargement()+ " | "
						));

			}

			document.add(chapter);
			document.add(Listu);
			document.add(chapter1);
			document.add(Lists);
			
			document.close();



		} catch (DocumentException e) {
		} catch (FileNotFoundException e) {
		}

	}
}

	/*public static void generatePDFListS(String path, java.util.List<StatistiquesVideo> ls) {
		Document document = new Document();
		File MyFile = new File(path);
		MyFile.delete();

		try {
			PdfWriter.getInstance(document, new FileOutputStream(path, false));



			document.open();
			//Cr�e un titre Audit

			Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
			Chunk chunk = new Chunk("Statistique", chapterFont);
			Chapter chapter = new Chapter(new Paragraph(chunk), 1);
			chapter.setNumberDepth(0);

			List ListStat = new List(List.ORDERED);
			for (StatistiquesVideo u : ls) {
				ListStat.add(new ListItem("| "
						
						+ u.getNbVue() + " | " 
						+ u.getNbTelechargement()+ " | "
						));
				document.add(chapter);
				document.add(ListStat);
				document.close();
			}
		}catch (DocumentException e) {
		} catch (FileNotFoundException e) {
		}

	}
}*/
/*public boolean createListUser(String filename)throws DocumentException, IOException, SQLException {

            ConnexionDB conn = new ConnexionDB();                
            Statement stmt = conn.createStatement();

            ResultSet res = stmt.executeQuery("SELECT id_utilisateur, nom, prenom, adresse FROM UTILISATEUR");

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("C:/Users/Lanson/Documents/netflox/code/resources/"+ filename+".pdf"));
            document.open();            

            PdfPTable table = new PdfPTable(4);

            PdfPCell table_cell;

            while (res.next()) {                
                            String id_utilisateur = res.getString("id_utilisateur");
                            table_cell=new PdfPCell(new Phrase(id_utilisateur));
                            table.addCell(table_cell);
                            String nom=res.getString("nom");
                            table_cell=new PdfPCell(new Phrase(nom));
                            table.addCell(table_cell);
                            String prenom=res.getString("prenom");
                            table_cell=new PdfPCell(new Phrase(prenom));
                            table.addCell(table_cell);
                            String adresse=res.getString("adresse");
                            table_cell=new PdfPCell(new Phrase(adresse));
                            table.addCell(table_cell);
                            }

            document.add(table);                       
            document.close();


            res.close();
            stmt.close();
			return true; 
	}               
                /*public static void main(String[] args) throws Exception{      
                	File file = new File(DEST1);
                	file.getParentFile().mkdirs();
                	File file1 = new File(DEST2);
                	file1.getParentFile().mkdirs();
                	new test().createListVideo(DEST1);
                	new test().createListUser(DEST2);
        }*/

