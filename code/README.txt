### LES REQUIS
* Java JRE 8.71
* Les requêtes pour créer les tables se trouvent dans le fichier code\WebContent\resources\tables.sql
* Pour remplir la table VIDEO préalablement créée, il faut utiliser la requête "\copy video (titre, auteur, annee, prix, etat, categorie, genre, motscles, nom_img, synopsis, nom_video) FROM '[insérer le chemin vers le fichier videos.csv ici]\videos.csv' DELIMITER ';' csv;""
* Pareil pour remplir la table SERIE.
* Installer les librairies qui se trouvent dans le dossier WebContent\WEB-INF\lib

### ORGANISATION DU CODE
Le dossier src contient les packages suivant :
* controllers : contient toutes les classes contrôleurs
* libs : contient les classes concernant la génération de PDF et un outil permettant le hashage du mot de passe
* models : contient toutes les classes modèles
* beans : contient toutes les classes objets
* tools : contient la classe de connexion à la base de données
Le dossier WebContent contient les fichiers vues de notre site web.